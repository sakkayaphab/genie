package account

import (
	"encoding/json"
	"github.com/jinzhu/gorm"
	"math/rand"
	"strings"
	"time"
)

type Token struct {
	TokenID        uint64 `gorm:"primary_key;AUTO_INCREMENT;not null" json:"tokenid"`
	UserID        uint64 `gorm:"not null" json:"userid"`
	PrimaryToken string `gorm:"type:varchar(128);not null" json:"primarytoken"`
	SecondaryToken string `gorm:"type:varchar(128);not null" json:"secondarytoken"`
	OS           string `gorm:"type:varchar(256)" json:"os"`
	Browser      string `gorm:"type:varchar(256)" json:"browser"`
	LogTimeModel
}

func (cfg *Config) DeleteTokenByTokenID(tokenid uint64)  (*Token,error) {
	db, err := gorm.Open(cfg.TokenDB.Dialect, cfg.TokenDB.Args)
	if err != nil {
		panic("failed to connect database")
	}
	defer db.Close()

	// Migrate the schema
	db.AutoMigrate(&Token{})

	token := Token{}
	err = db.Where("token_id = ?",tokenid).Delete(&token).Error
	if err!=nil {
		return &Token{},err
	}

	return &token,err
}

func (cfg *Config) GetTokenByTokenID(tokenid uint64) ( *Token,error) {
	db, err := gorm.Open(cfg.TokenDB.Dialect, cfg.TokenDB.Args)
	if err != nil {
		panic("failed to connect database")
	}
	defer db.Close()

	// Migrate the schema
	db.AutoMigrate(&Token{})

	// Read
	var token Token
	err = db.First(&token, "token_id = ?", tokenid).Error
	if err!=nil {
		return &Token{},err
	}
	return &token,nil
}

func (cfg *Config) AddTokenByUserIDBrowserOs(userid uint64,browser string,os string) (*Token,error) {
	db, err := gorm.Open(cfg.TokenDB.Dialect, cfg.TokenDB.Args)
	if err != nil {
		panic("failed to connect database")
	}
	defer db.Close()
	// Migrate the schema
	db.AutoMigrate(&Token{})

	primaryToken := RandomString(128)
	secondaryToken := RandomString(128)

	token := Token{UserID: userid,PrimaryToken:primaryToken,SecondaryToken:secondaryToken,Browser:browser,OS:os}
	err = db.Create(&token).Error
	if err != nil{
		return &Token{},err
	}

	return &token,nil
}

func (cfg *Config) AddTokenByUserID(userid uint64) (*Token,error) {
	db, err := gorm.Open(cfg.TokenDB.Dialect, cfg.TokenDB.Args)
	if err != nil {
		panic("failed to connect database")
	}
	defer db.Close()
	// Migrate the schema
	db.AutoMigrate(&Token{})

	primaryToken := RandomString(128)
	secondaryToken := RandomString(128)

	token := Token{UserID: userid,PrimaryToken:primaryToken,SecondaryToken:secondaryToken}
	err = db.Create(&token).Error
	if err != nil{
		return &Token{},err
	}

	return &token,nil
}

func RandomString(length int) string {
	rand.Seed(time.Now().UnixNano())
	chars := []rune("ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
		"abcdefghijklmnopqrstuvwxyz" +
		"0123456789")
	var b strings.Builder
	for i := 0; i < length; i++ {
		b.WriteRune(chars[rand.Intn(len(chars))])
	}
	str := b.String()
	return str
}

func (cfg *Config) GetAllToken() (*[]Token,error) {
	db, err := gorm.Open(cfg.TokenDB.Dialect, cfg.TokenDB.Args)
	if err != nil {
		panic("failed to connect database")
	}
	defer db.Close()

	// Migrate the schema
	db.AutoMigrate(&Token{})

	// Read
	var tokens []Token
	err = db.Find(&tokens).Error
	if err!=nil {
		return &tokens,err
	}
	return &tokens,nil
}

func (cfg *Config) GetAllTokenByUserID(userid uint64) (*[]Token,error) {
	db, err := gorm.Open(cfg.TokenDB.Dialect, cfg.TokenDB.Args)
	if err != nil {
		panic("failed to connect database")
	}
	defer db.Close()

	// Migrate the schema
	db.AutoMigrate(&Token{})

	// Read
	var tokens []Token
	err = db.Where("userid = ?", userid).Find(&tokens).Error
	if err!=nil {
		return &tokens,err
	}
	return &tokens,nil
}

func (token *Token) ToPrettyJson()  (string,error) {
	text, err := json.MarshalIndent(token, "", " ")
	if err!=nil {
		return  "",err
	}
	return  string(text),nil
}
