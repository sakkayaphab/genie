package account

import (
	"encoding/json"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"time"
)

type Config struct {
	TokenDB  struct {
		Dialect string `json:"dialect"`
		Args string `json:"args"`
	} `json:"tokendb"`
	AccountDB  struct {
		Dialect string `json:"dialect"`
		Args string `json:"args"`
	} `json:"accountdb"`
}

func NewConfig()  *Config{
	return &Config{}
}

type LogTimeModel struct {
	CreatedAt time.Time `json:"createdat"`
	UpdatedAt time.Time `json:"updatedat"`
	DeletedAt *time.Time `json:"deletedat"`
}

type Profile struct {
	UserID        uint64 `gorm:"primary_key;AUTO_INCREMENT;not null" json:"userid"`
	Username string `gorm:"type:varchar(255);unique_index;not null" json:"username"`
	Password string `gorm:"type:varchar(255);not null" json:"password"`
	LogTimeModel
}

func (cfg *Config)AddProfileByUsernamePassword(username , password string) (*Profile,error) {
	db, err := gorm.Open(cfg.AccountDB.Dialect, cfg.AccountDB.Args)
	if err != nil {
		panic("failed to connect database")
	}
	defer db.Close()

	// Migrate the schema
	db.AutoMigrate(&Profile{})

	profile := Profile{Username: username, Password:password}
	// Create
	err = db.Create(&profile).Error
	if err != nil{
		return &Profile{},err
	}

	return &profile,nil
}

func (cfg *Config) GetProfileByUsername(username string) (*Profile,error) {
	db, err := gorm.Open(cfg.AccountDB.Dialect, cfg.AccountDB.Args)
	if err != nil {
		panic("failed to connect database")
	}
	defer db.Close()

	// Migrate the schema
	db.AutoMigrate(&Profile{})

	// Read
	var profile Profile
	err = db.First(&profile, "username = ?", username).Error
	if err!=nil {
		return &Profile{},err
	}

	return &profile,nil
}

func (cfg *Config)GetProfileByUserID(userid uint64) (*Profile,error) {
	db, err := gorm.Open(cfg.AccountDB.Dialect, cfg.AccountDB.Args)
	if err != nil {
		panic("failed to connect database")
	}
	defer db.Close()

	// Migrate the schema
	db.AutoMigrate(&Token{})

	// Read
	var profile Profile
	err = db.First(&profile, "user_id = ?", userid).Error
	if err!=nil {
		return &Profile{},err
	}

	return &profile,nil
}

func (cfg *Config)ChangePassword(profile *Profile,password string) (*Profile,error) {
	db, err := gorm.Open(cfg.AccountDB.Dialect, cfg.AccountDB.Args)
	if err != nil {
		panic("failed to connect database")
	}
	defer db.Close()

	// Migrate the schema
	db.AutoMigrate(&Token{})

	err = db.Model(&profile).Update("password", password).Error
	if err!=nil {
		return &Profile{},err
	}

	return profile,nil
}

func (cfg *Config)GetAllProfile() (*[]Profile,error) {
	db, err := gorm.Open(cfg.AccountDB.Dialect, cfg.AccountDB.Args)
	if err != nil {
		panic("failed to connect database")
	}
	defer db.Close()

	// Migrate the schema
	db.AutoMigrate(&Token{})

	// Read
	var profiles []Profile
	err = db.Find(&profiles).Error
	if err!=nil {
		return &profiles,err
	}

	return &profiles,nil
}

func (profile *Profile) ToPrettyJson()  (string,error) {
	text, err := json.MarshalIndent(profile, "", " ")
	if err!=nil {
		return  "",err
	}
	return  string(text),nil
}
