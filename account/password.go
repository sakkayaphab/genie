package account

import (
	"crypto/hmac"
	"crypto/sha512"
	"encoding/hex"
)


func GenerateSHA512HMACHash(data string) string{
	secret := "genie"

	// Create a new HMAC by defining the hash type and the key (as byte array)
	h := hmac.New(sha512.New, []byte(secret))

	// Write Data to it
	h.Write([]byte(data))

	// Get result and encode as hexadecimal string
	sha := hex.EncodeToString(h.Sum(nil))

	return  sha
}