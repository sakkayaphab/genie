package gff

import (
	"bufio"
	"fmt"
	"io"
	"os"
)

type ConvertConfig struct {
	FilePath   string
	Output     string
	SeqID      bool
	Source     bool
	TypeData   bool
	Start      bool
	End        bool
	Score      bool
	Strand     bool
	Phase      bool
	Attributes []string
	Header     bool
	Limit int64
	Separator string
	Replacement string
}

func NewCsvConfig() *ConvertConfig {
	return &ConvertConfig{}
}

func (csvconfig *ConvertConfig) ConvertToCSV() error {
	file, err := os.Open(csvconfig.FilePath)
	if err != nil {
		return err
	}
	defer file.Close()

	r := bufio.NewReaderSize(file, 1024*10000)
	var count int64
	for {

		line, _, err := r.ReadLine()
		if err == io.EOF {
			break
		}

		if string(line)[0:1] == "#" {
			continue
		}

		text := string(line)
		var record Record
		record.Source = &text

		var tempText string
		firstadd := true
		if csvconfig.SeqID {
			addValueCSV(&tempText, record.GetSeqID(), &firstadd, csvconfig.Replacement, csvconfig.Separator)
		}
		if csvconfig.Source {
			addValueCSV(&tempText, record.GetSource(), &firstadd, csvconfig.Replacement, csvconfig.Separator)
		}
		if csvconfig.TypeData {
			addValueCSV(&tempText, record.GetType(), &firstadd, csvconfig.Replacement, csvconfig.Separator)
		}
		if csvconfig.Start {
			addValueCSV(&tempText, record.GetStartString(), &firstadd, csvconfig.Replacement, csvconfig.Separator)
		}
		if csvconfig.End {
			addValueCSV(&tempText, record.GetEndString(), &firstadd, csvconfig.Replacement, csvconfig.Separator)
		}
		if csvconfig.Score {
			addValueCSV(&tempText, record.GetScore(), &firstadd, csvconfig.Replacement, csvconfig.Separator)
		}
		if csvconfig.Strand {
			addValueCSV(&tempText, record.GetStrand(), &firstadd, csvconfig.Replacement, csvconfig.Separator)
		}
		if csvconfig.Phase {
			addValueCSV(&tempText, record.GetPhase(), &firstadd, csvconfig.Replacement, csvconfig.Separator)
		}
		if len(csvconfig.Attributes) > 0 {
			mAttributes := record.GetMapAttributes()
			for _, x := range csvconfig.Attributes {
				addValueCSV(&tempText, mAttributes[x], &firstadd, csvconfig.Replacement, csvconfig.Separator)
			}
		}

		if tempText != "" {
			fmt.Println(tempText)
		}

		count++

		if csvconfig.Limit > -1 {
			if csvconfig.Limit<=count {
				break
			}
		}

	}

	return nil
}

func (csvconfig *ConvertConfig) ShowHeader(replacement, spliter string) {
	var tempText string
	firstadd := true
	if csvconfig.SeqID {
		addValueCSV(&tempText, "seqid", &firstadd, replacement, spliter)
	}
	if csvconfig.Source {
		addValueCSV(&tempText, "source", &firstadd, replacement, spliter)
	}
	if csvconfig.TypeData {
		addValueCSV(&tempText, "type", &firstadd, replacement, spliter)
	}
	if csvconfig.Start {
		addValueCSV(&tempText, "start", &firstadd, replacement, spliter)
	}
	if csvconfig.End {
		addValueCSV(&tempText, "end", &firstadd, replacement, spliter)
	}
	if csvconfig.Score {
		addValueCSV(&tempText, "score", &firstadd, replacement, spliter)
	}
	if csvconfig.Strand {
		addValueCSV(&tempText, "strand", &firstadd, replacement, spliter)
	}
	if csvconfig.Phase {
		addValueCSV(&tempText, "phase", &firstadd, replacement, spliter)
	}
	if len(csvconfig.Attributes) > 0 {
		for _, x := range csvconfig.Attributes {
			addValueCSV(&tempText, x, &firstadd, replacement, spliter)
		}
	}

	if tempText != "" {
		fmt.Println(tempText)
	}
}

func addValueCSV(text *string, insert string, firstadd *bool, replacement, spliter string) {
	if insert == "" {
		insert = replacement
	}
	if *firstadd {
		*text += insert
		*firstadd = false
	} else {
		*text += spliter + insert
	}
}
