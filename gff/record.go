package gff

import (
	"strings"
)

type Record struct {
	Source *string
}

func (r *Record) SplitSourceTab() *[]string {
	text := strings.Split(*r.Source, "\t")
	return &text
}

func (r *Record) GetSeqID() string {
	return (*r.SplitSourceTab())[0]
}

func (r *Record) GetSource() string {
	return (*r.SplitSourceTab())[1]
}

func (r *Record) GetType() string {
	return (*r.SplitSourceTab())[2]
}

func (r *Record) GetStartString() string {
	return (*r.SplitSourceTab())[3]
}

func (r *Record) GetEndString() string {
	return (*r.SplitSourceTab())[4]
}

func (r *Record) GetScore() string {
	return (*r.SplitSourceTab())[5]
}

func (r *Record) GetStrand() string {
	return (*r.SplitSourceTab())[6]
}

func (r *Record) GetPhase() string {
	return (*r.SplitSourceTab())[7]
}

func (r *Record) GetAttributes() string {
	return (*r.SplitSourceTab())[8]
}

func (r *Record) GetMapAttributes() map[string]string {
	var m = make(map[string]string)

	texts := strings.Split(r.GetAttributes(), ";")
	for _, y := range texts {
		keyvaluelists := strings.Split(y, "=")
		m[keyvaluelists[0]] = keyvaluelists[1]
	}

	return m
}
