package bed

import (
	"bufio"
	"io"
	"log"
	"os"
)

type BED struct {
	Record []Record
}

func ReadFile(filepath string) (BED, error) {
	file, err := os.Open(filepath)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	reader := bufio.NewReaderSize(file, 1024*10000)
	var bed BED
	for {
		line, _, err := reader.ReadLine()

		if err == io.EOF {
			break
		}

		if string(line)[0:1] == "#" {
			continue
		}

		record, err := ReadLineRecord(string(line))
		if err != nil {
			log.Fatal(err)
		}
		bed.Record = append(bed.Record, record)
	}

	return bed, nil
}
