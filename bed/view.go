package bed

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
)

type BEDViewer struct {
	FilePath string
}


func NewViewer()  *BEDViewer{
	return &BEDViewer{}
}

func (vbed *BEDViewer) ViewBED()  error{
	file, err := os.Open(vbed.FilePath)
	if err != nil {
		if err != nil {
			return err
		}
	}
	defer file.Close()
	r := bufio.NewReaderSize(file, 1024*10000)
	for {
		line, _, err := r.ReadLine()
		if err == io.EOF {
			break
		}

		s := strings.Split(string(line), "\t")
		if len(s)>=3 {
			fmt.Println(string(line))
		}

	}
	return nil
}

func (vbed *BEDViewer) CountBED()  (int64,error){
	file, err := os.Open(vbed.FilePath)
	if err != nil {
		if err != nil {
			return 0,err
		}
	}
	defer file.Close()
	r := bufio.NewReaderSize(file, 1024*10000)
	var count int64
	for {
		line, _, err := r.ReadLine()
		if err == io.EOF {
			break
		}

		s := strings.Split(string(line), "\t")
		if len(s)>=3 {
			count++
		}

	}
	return count,nil
}