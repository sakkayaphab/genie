package bed

import (
	"errors"
	"strconv"
	"strings"
)

type Record struct {
	//Require
	Chrom      string
	ChromStart int
	ChromEnd   int
}

func (r *Record) GetChrom() string {
	return r.Chrom
}

func (r *Record) GetChromStart() int {
	return r.ChromStart
}

func (r *Record) GetChromEnd() int {
	return r.ChromEnd
}

func ReadLineRecord(line string) (Record, error) {
	var r Record

	s := strings.Split(string(line), "\t")
	if len(s) < 3 {
		return Record{}, errors.New("not VCF Format")
	}

	r.Chrom = s[0]
	pos, err := strconv.Atoi(s[1])
	if err != nil {
		return Record{}, errors.New("POS can't parsed")
	}

	r.ChromStart = pos

	posEnd, err := strconv.Atoi(s[2])
	if err != nil {
		return Record{}, errors.New("POS can't parsed")
	}

	r.ChromEnd = posEnd

	return r, nil
}
