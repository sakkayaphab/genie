package bed

import (
	"log"
	"testing"
)

func TestFastaReaderGenarateFaidx(t *testing.T) {
	bedviewer := NewViewer()
	bedviewer.FilePath = "testdata/demo.bed"
	count,err := bedviewer.CountBED()
	if err!=nil {
		log.Fatal(err)
	}

	if count!=7 {
		t.Errorf("count is invalid")
	}
}