package gridengine

import (
	"encoding/json"
	"encoding/xml"
	"log"
	"testing"
)



func TestQhost(t *testing.T) {

	str := `<?xml version='1.0'?>
<qhost xmlns:xsd="http://arc.liv.ac.uk/repos/darcs/sge/source/dist/util/resources/schemas/qhost/qhost.xsd">
 <host name='global'>
   <hostvalue name='arch_string'>-</hostvalue>
   <hostvalue name='num_proc'>-</hostvalue>
   <hostvalue name='m_socket'>-</hostvalue>
   <hostvalue name='m_core'>-</hostvalue>
   <hostvalue name='m_thread'>-</hostvalue>
   <hostvalue name='load_avg'>-</hostvalue>
   <hostvalue name='mem_total'>-</hostvalue>
   <hostvalue name='mem_used'>-</hostvalue>
   <hostvalue name='swap_total'>-</hostvalue>
   <hostvalue name='swap_used'>-</hostvalue>
 </host>
 <host name='compute-01-10g'>
   <hostvalue name='arch_string'>lx-amd64</hostvalue>
   <hostvalue name='num_proc'>20</hostvalue>
   <hostvalue name='m_socket'>2</hostvalue>
   <hostvalue name='m_core'>20</hostvalue>
   <hostvalue name='m_thread'>20</hostvalue>
   <hostvalue name='load_avg'>0.11</hostvalue>
   <hostvalue name='mem_total'>93.6G</hostvalue>
   <hostvalue name='mem_used'>1.3G</hostvalue>
   <hostvalue name='swap_total'>32.0G</hostvalue>
   <hostvalue name='swap_used'>35.0M</hostvalue>
   <resourcevalue name='arch' dominance='hl'>lx-amd64</resourcevalue>
   <resourcevalue name='num_proc' dominance='hl'>20.000000</resourcevalue>
   <resourcevalue name='mem_total' dominance='hl'>93.573G</resourcevalue>
   <resourcevalue name='swap_total' dominance='hl'>32.000G</resourcevalue>
   <resourcevalue name='virtual_total' dominance='hl'>125.573G</resourcevalue>
   <resourcevalue name='m_topology' dominance='hl'>SCCCCCCCCCCSCCCCCCCCCC</resourcevalue>
   <resourcevalue name='m_socket' dominance='hl'>2.000000</resourcevalue>
   <resourcevalue name='m_core' dominance='hl'>20.000000</resourcevalue>
   <resourcevalue name='m_thread' dominance='hl'>20.000000</resourcevalue>
   <resourcevalue name='load_avg' dominance='hl'>0.110000</resourcevalue>
   <resourcevalue name='load_short' dominance='hl'>0.040000</resourcevalue>
   <resourcevalue name='load_medium' dominance='hl'>0.110000</resourcevalue>
   <resourcevalue name='load_long' dominance='hl'>0.130000</resourcevalue>
   <resourcevalue name='mem_free' dominance='hl'>92.299G</resourcevalue>
   <resourcevalue name='swap_free' dominance='hl'>31.966G</resourcevalue>
   <resourcevalue name='virtual_free' dominance='hl'>124.264G</resourcevalue>
   <resourcevalue name='mem_used' dominance='hl'>1.274G</resourcevalue>
   <resourcevalue name='swap_used' dominance='hl'>35.016M</resourcevalue>
   <resourcevalue name='virtual_used' dominance='hl'>1.308G</resourcevalue>
   <resourcevalue name='cpu' dominance='hl'>0.000000</resourcevalue>
   <resourcevalue name='m_topology_inuse' dominance='hl'>SCCCCCCCCCCSCCCCCCCCCC</resourcevalue>
   <resourcevalue name='np_load_avg' dominance='hl'>0.005500</resourcevalue>
   <resourcevalue name='np_load_short' dominance='hl'>0.002000</resourcevalue>
   <resourcevalue name='np_load_medium' dominance='hl'>0.005500</resourcevalue>
   <resourcevalue name='np_load_long' dominance='hl'>0.006500</resourcevalue>
 </host>
 <host name='compute-02-10g'>
   <hostvalue name='arch_string'>lx-amd64</hostvalue>
   <hostvalue name='num_proc'>20</hostvalue>
   <hostvalue name='m_socket'>2</hostvalue>
   <hostvalue name='m_core'>20</hostvalue>
   <hostvalue name='m_thread'>20</hostvalue>
   <hostvalue name='load_avg'>0.07</hostvalue>
   <hostvalue name='mem_total'>93.6G</hostvalue>
   <hostvalue name='mem_used'>1.6G</hostvalue>
   <hostvalue name='swap_total'>32.0G</hostvalue>
   <hostvalue name='swap_used'>6.1M</hostvalue>
   <resourcevalue name='arch' dominance='hl'>lx-amd64</resourcevalue>
   <resourcevalue name='num_proc' dominance='hl'>20.000000</resourcevalue>
   <resourcevalue name='mem_total' dominance='hl'>93.573G</resourcevalue>
   <resourcevalue name='swap_total' dominance='hl'>32.000G</resourcevalue>
   <resourcevalue name='virtual_total' dominance='hl'>125.573G</resourcevalue>
   <resourcevalue name='m_topology' dominance='hl'>SCCCCCCCCCCSCCCCCCCCCC</resourcevalue>
   <resourcevalue name='m_socket' dominance='hl'>2.000000</resourcevalue>
   <resourcevalue name='m_core' dominance='hl'>20.000000</resourcevalue>
   <resourcevalue name='m_thread' dominance='hl'>20.000000</resourcevalue>
   <resourcevalue name='load_avg' dominance='hl'>0.070000</resourcevalue>
   <resourcevalue name='load_short' dominance='hl'>0.140000</resourcevalue>
   <resourcevalue name='load_medium' dominance='hl'>0.070000</resourcevalue>
   <resourcevalue name='load_long' dominance='hl'>0.110000</resourcevalue>
   <resourcevalue name='mem_free' dominance='hl'>91.941G</resourcevalue>
   <resourcevalue name='swap_free' dominance='hl'>31.994G</resourcevalue>
   <resourcevalue name='virtual_free' dominance='hl'>123.935G</resourcevalue>
   <resourcevalue name='mem_used' dominance='hl'>1.631G</resourcevalue>
   <resourcevalue name='swap_used' dominance='hl'>6.133M</resourcevalue>
   <resourcevalue name='virtual_used' dominance='hl'>1.637G</resourcevalue>
   <resourcevalue name='cpu' dominance='hl'>0.000000</resourcevalue>
   <resourcevalue name='m_topology_inuse' dominance='hl'>SCCCCCCCCCCSCCCCCCCCCC</resourcevalue>
   <resourcevalue name='np_load_avg' dominance='hl'>0.003500</resourcevalue>
   <resourcevalue name='np_load_short' dominance='hl'>0.007000</resourcevalue>
   <resourcevalue name='np_load_medium' dominance='hl'>0.003500</resourcevalue>
   <resourcevalue name='np_load_long' dominance='hl'>0.005500</resourcevalue>
 </host>
 <host name='compute-03-10g'>
   <hostvalue name='arch_string'>lx-amd64</hostvalue>
   <hostvalue name='num_proc'>20</hostvalue>
   <hostvalue name='m_socket'>2</hostvalue>
   <hostvalue name='m_core'>20</hostvalue>
   <hostvalue name='m_thread'>20</hostvalue>
   <hostvalue name='load_avg'>0.05</hostvalue>
   <hostvalue name='mem_total'>93.6G</hostvalue>
   <hostvalue name='mem_used'>1.3G</hostvalue>
   <hostvalue name='swap_total'>32.0G</hostvalue>
   <hostvalue name='swap_used'>32.0M</hostvalue>
   <resourcevalue name='arch' dominance='hl'>lx-amd64</resourcevalue>
   <resourcevalue name='num_proc' dominance='hl'>20.000000</resourcevalue>
   <resourcevalue name='mem_total' dominance='hl'>93.573G</resourcevalue>
   <resourcevalue name='swap_total' dominance='hl'>32.000G</resourcevalue>
   <resourcevalue name='virtual_total' dominance='hl'>125.573G</resourcevalue>
   <resourcevalue name='m_topology' dominance='hl'>SCCCCCCCCCCSCCCCCCCCCC</resourcevalue>
   <resourcevalue name='m_socket' dominance='hl'>2.000000</resourcevalue>
   <resourcevalue name='m_core' dominance='hl'>20.000000</resourcevalue>
   <resourcevalue name='m_thread' dominance='hl'>20.000000</resourcevalue>
   <resourcevalue name='load_avg' dominance='hl'>0.050000</resourcevalue>
   <resourcevalue name='load_short' dominance='hl'>0.000000</resourcevalue>
   <resourcevalue name='load_medium' dominance='hl'>0.050000</resourcevalue>
   <resourcevalue name='load_long' dominance='hl'>0.100000</resourcevalue>
   <resourcevalue name='mem_free' dominance='hl'>92.322G</resourcevalue>
   <resourcevalue name='swap_free' dominance='hl'>31.969G</resourcevalue>
   <resourcevalue name='virtual_free' dominance='hl'>124.290G</resourcevalue>
   <resourcevalue name='mem_used' dominance='hl'>1.251G</resourcevalue>
   <resourcevalue name='swap_used' dominance='hl'>32.047M</resourcevalue>
   <resourcevalue name='virtual_used' dominance='hl'>1.282G</resourcevalue>
   <resourcevalue name='cpu' dominance='hl'>0.000000</resourcevalue>
   <resourcevalue name='m_topology_inuse' dominance='hl'>SCCCCCCCCCCSCCCCCCCCCC</resourcevalue>
   <resourcevalue name='np_load_avg' dominance='hl'>0.002500</resourcevalue>
   <resourcevalue name='np_load_short' dominance='hl'>0.000000</resourcevalue>
   <resourcevalue name='np_load_medium' dominance='hl'>0.002500</resourcevalue>
   <resourcevalue name='np_load_long' dominance='hl'>0.005000</resourcevalue>
 </host>
 <host name='compute-04-10g'>
   <hostvalue name='arch_string'>lx-amd64</hostvalue>
   <hostvalue name='num_proc'>20</hostvalue>
   <hostvalue name='m_socket'>2</hostvalue>
   <hostvalue name='m_core'>20</hostvalue>
   <hostvalue name='m_thread'>20</hostvalue>
   <hostvalue name='load_avg'>0.07</hostvalue>
   <hostvalue name='mem_total'>93.6G</hostvalue>
   <hostvalue name='mem_used'>1.5G</hostvalue>
   <hostvalue name='swap_total'>32.0G</hostvalue>
   <hostvalue name='swap_used'>34.0M</hostvalue>
   <resourcevalue name='arch' dominance='hl'>lx-amd64</resourcevalue>
   <resourcevalue name='num_proc' dominance='hl'>20.000000</resourcevalue>
   <resourcevalue name='mem_total' dominance='hl'>93.572G</resourcevalue>
   <resourcevalue name='swap_total' dominance='hl'>32.000G</resourcevalue>
   <resourcevalue name='virtual_total' dominance='hl'>125.572G</resourcevalue>
   <resourcevalue name='m_topology' dominance='hl'>SCCCCCCCCCCSCCCCCCCCCC</resourcevalue>
   <resourcevalue name='m_socket' dominance='hl'>2.000000</resourcevalue>
   <resourcevalue name='m_core' dominance='hl'>20.000000</resourcevalue>
   <resourcevalue name='m_thread' dominance='hl'>20.000000</resourcevalue>
   <resourcevalue name='load_avg' dominance='hl'>0.070000</resourcevalue>
   <resourcevalue name='load_short' dominance='hl'>0.000000</resourcevalue>
   <resourcevalue name='load_medium' dominance='hl'>0.070000</resourcevalue>
   <resourcevalue name='load_long' dominance='hl'>0.110000</resourcevalue>
   <resourcevalue name='mem_free' dominance='hl'>92.076G</resourcevalue>
   <resourcevalue name='swap_free' dominance='hl'>31.967G</resourcevalue>
   <resourcevalue name='virtual_free' dominance='hl'>124.043G</resourcevalue>
   <resourcevalue name='mem_used' dominance='hl'>1.496G</resourcevalue>
   <resourcevalue name='swap_used' dominance='hl'>33.953M</resourcevalue>
   <resourcevalue name='virtual_used' dominance='hl'>1.530G</resourcevalue>
   <resourcevalue name='cpu' dominance='hl'>0.000000</resourcevalue>
   <resourcevalue name='m_topology_inuse' dominance='hl'>SCCCCCCCCCCSCCCCCCCCCC</resourcevalue>
   <resourcevalue name='np_load_avg' dominance='hl'>0.003500</resourcevalue>
   <resourcevalue name='np_load_short' dominance='hl'>0.000000</resourcevalue>
   <resourcevalue name='np_load_medium' dominance='hl'>0.003500</resourcevalue>
   <resourcevalue name='np_load_long' dominance='hl'>0.005500</resourcevalue>
 </host>
</qhost>
`

	var qhost QHost
	err := xml.Unmarshal([]byte(str), &qhost)
	if err != nil {
		log.Fatalf("xml.Unmarshal failed with '%s'\n", err)
	}


	_, err = json.MarshalIndent(&qhost, "", "    ")
	if err != nil {
		log.Fatal("Failed to generate json", err)
	}

	if err != nil {
		log.Fatal(err)
	}

	//fmt.Println(string(prettyJSON))
}
