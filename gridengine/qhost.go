package gridengine

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"log"
	"os"
	"os/exec"
)


type QHost struct {
	XMLName xml.Name `xml:"qhost" json:"qhost"`
	Xsd     string   `xml:"xsd,attr" json:"xsd,attr"`
	Host    []struct {
		Name      string `xml:"name,attr json:"name"`
		Hostvalue []struct {
			Text string `xml:",chardata json:"text"`
			Name string `xml:"name,attr" json:"name"`
		} `xml:"hostvalue" json:"hostvalue"`
		Resourcevalue []struct {
			Text      string `xml:",chardata" json:"text"`
			Name      string `xml:"name,attr" json:"name"`
			Dominance string `xml:"dominance,attr" json:"dominance"`
		} `xml:"resourcevalue" json:"resourcevalue"`
	} `xml:"host" json:"host"`
}

func (qhost *QHost) ToXML()  (string,error) {
	d, err := xml.Marshal(qhost)
	if err != nil {
		log.Fatalf("xml.Marshal failed with '%s'\n", err)
	}
	return string(d),err
}

func (qhost *QHost) ToPrettyXML()  (string,error){

	d, err := xml.MarshalIndent(&qhost, "", "  ")
	if err != nil {
		log.Fatalf("xml.MarshalIndent failed with '%s'\n", err)
	}
	fmt.Printf("Pretty printed XML:\n%s\n", string(d))

	return string(d),err
}

func (qhost *QHost) ToJson()  (string,error){

	jsonData, err := json.Marshal(qhost)

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	return string(jsonData),err
}

func (qhost *QHost) ToPrettyJson()  (string,error){

	prettyJSON, err := json.MarshalIndent(qhost, "", "    ")
	if err != nil {
		log.Fatal("Failed to generate json", err)
	}

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	return string(prettyJSON),err
}

func GetQHost()  (*QHost,error) {
	cmd := exec.Command("qhost","-F","-xml")
	out,err := cmd.Output()
	if err!=nil {
		return &QHost{},err
	}

	var qhost QHost
	err = xml.Unmarshal(out, &qhost)
	if err != nil {
		log.Fatalf("xml.Unmarshal failed with '%s'\n", err)
	}
	//fmt.Println(qhost.Host[1])

	return  &qhost,nil
}