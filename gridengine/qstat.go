package gridengine

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"log"
	"os"
	"os/exec"
)

type QStat struct {
	XMLName   xml.Name `xml:"job_info" json:"jobinfo"`
	Xsd       string   `xml:"xsd,attr" json:"xsd"`
	QueueInfo struct {
		QueueList []struct {
			Text       string `xml:",chardata" json:"text"`
			Name       string `xml:"name" json:"name"`
			Qtype      string `xml:"qtype" json:"qtype"`
			SlotsUsed  string `xml:"slots_used" json:"slotsused"`
			SlotsResv  string `xml:"slots_resv" json:"slotsresv"`
			SlotsTotal string `xml:"slots_total" json:"slotstotal"`
			LoadAvg    string `xml:"load_avg" json:"loadavg"`
			Arch       string `xml:"arch" json:"arch"`
			JobList    []struct {
				Text         string `xml:",chardata" json:"text"`
				AttrState    string `xml:"state,attr" json:"attrstate"`
				JBJobNumber  string `xml:"JB_job_number" json:"jbjobnumber"`
				JATPrio      string `xml:"JAT_prio" json:"jatprio"`
				JBName       string `xml:"JB_name" json:"jbname"`
				JBOwner      string `xml:"JB_owner" json:"jbowner"`
				State        string `xml:"state" json:"state"`
				JATStartTime string `xml:"JAT_start_time" json:"jatstarttime"`
				Slots        string `xml:"slots" json:"slots"`
			} `xml:"job_list" json:"joblist"`
		} `xml:"Queue-List" json:"queuelist"`
	} `xml:"queue_info" json:"queueinfo"`
	JobInfo struct {
		JobList []struct {
			Text             string `xml:",chardata" json:"text"`
			AttrState        string `xml:"state,attr" json:"attrstate"`
			JBJobNumber      string `xml:"JB_job_number" json:"jbjobnumber"`
			JATPrio          string `xml:"JAT_prio" json:"jatprio"`
			JBName           string `xml:"JB_name" json:"jbname"`
			JBOwner          string `xml:"JB_owner" json:"jbowner"`
			State            string `xml:"state" json:"state"`
			JBSubmissionTime string `xml:"JB_submission_time" json:"jbsubmissiontime"`
			Slots            string `xml:"slots" json:"slots"`
		} `xml:"job_list" json:"joblist"`
	} `xml:"job_info" json:"jobinfo"`
}


func (qstat *QStat) ToXML()  (string,error) {
	d, err := xml.Marshal(qstat)
	if err != nil {
		log.Fatalf("xml.Marshal failed with '%s'\n", err)
	}
	return string(d),err
}

func (qstat *QStat) ToPrettyXML()  (string,error){

	d, err := xml.MarshalIndent(&qstat, "", "  ")
	if err != nil {
		log.Fatalf("xml.MarshalIndent failed with '%s'\n", err)
	}
	fmt.Printf("Pretty printed XML:\n%s\n", string(d))

	return string(d),err
}

func (qstat *QStat) ToJson()  (string,error){

	jsonData, err := json.Marshal(qstat)

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	return string(jsonData),err
}

func (qstat *QStat) ToPrettyJson()  (string,error){

	prettyJSON, err := json.MarshalIndent(qstat, "", "    ")
	if err != nil {
		log.Fatal("Failed to generate json", err)
	}

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	return string(prettyJSON),err
}

func GetQStat()  (*QStat,error) {
	cmd := exec.Command("qstat", "-f", "-xml")
	out, err := cmd.Output()
	if err != nil {
		return &QStat{}, err
	}

	var qstat QStat
	err = xml.Unmarshal(out, &qstat)
	if err != nil {
		log.Fatalf("xml.Unmarshal failed with '%s'\n", err)
	}

	return &qstat, nil
}