package gridengine

import (
	"encoding/json"
	"encoding/xml"
	"log"
	"testing"
)


func TestQstat(t *testing.T) {
	strJobInfo := `<job_info  xmlns:xsd="http://arc.liv.ac.uk/repos/darcs/sge/source/dist/util/resources/schemas/qstat/qstat.xsd">
  <queue_info>
    <Queue-List>
      <name>all.q@compute-01-10g</name>
      <qtype>BIP</qtype>
      <slots_used>1</slots_used>
      <slots_resv>0</slots_resv>
      <slots_total>20</slots_total>
      <load_avg>0.07000</load_avg>
      <arch>lx-amd64</arch>
    </Queue-List>
    <Queue-List>
      <name>all.q@compute-02-10g</name>
      <qtype>BIP</qtype>
      <slots_used>1</slots_used>
      <slots_resv>0</slots_resv>
      <slots_total>20</slots_total>
      <load_avg>0.15000</load_avg>
      <arch>lx-amd64</arch>
    </Queue-List>
    <Queue-List>
      <name>all.q@compute-03-10g</name>
      <qtype>BIP</qtype>
      <slots_used>0</slots_used>
      <slots_resv>0</slots_resv>
      <slots_total>20</slots_total>
      <load_avg>0.17000</load_avg>
      <arch>lx-amd64</arch>
    </Queue-List>
     <Queue-List>
      <name>all.q@compute-04-10g</name>
      <qtype>BIP</qtype>
      <slots_used>2</slots_used>
      <slots_resv>0</slots_resv>
      <slots_total>20</slots_total>
      <load_avg>0.13000</load_avg>
      <arch>lx-amd64</arch>
      <job_list state="running">
        <JB_job_number>12769</JB_job_number>
        <JAT_prio>0.55500</JAT_prio>
        <JB_name>sleep</JB_name>
        <JB_owner>duangdao</JB_owner>
        <state>r</state>
        <JAT_start_time>2019-12-18T17:25:02</JAT_start_time>
        <slots>1</slots>
      </job_list>
    </Queue-List>
  </queue_info>
  <job_info>
    <job_list state="pending">
      <JB_job_number>12773</JB_job_number>
      <JAT_prio>0.00000</JAT_prio>
      <JB_name>sleep</JB_name>
      <JB_owner>duangdao</JB_owner>
      <state>qw</state>
      <JB_submission_time>2019-12-18T17:27:02</JB_submission_time>
      <slots>1</slots>
    </job_list>
 	<job_list state="pending">
      <JB_job_number>12774</JB_job_number>
      <JAT_prio>0.00000</JAT_prio>
      <JB_name>sleep</JB_name>
      <JB_owner>duangdao</JB_owner>
      <state>qw</state>
      <JB_submission_time>2019-12-18T17:27:02</JB_submission_time>
      <slots>1</slots>
    </job_list>
  </job_info>
</job_info>`

	var qstat QStat
	err := xml.Unmarshal([]byte(strJobInfo), &qstat)
	if err != nil {
		log.Fatalf("xml.Unmarshal failed with '%s'\n", err)
	}

	_, err = json.MarshalIndent(&qstat, "", "    ")
	if err != nil {
		log.Fatal("Failed to generate json", err)
	}

	if err != nil {
		log.Fatal(err)
	}

}
