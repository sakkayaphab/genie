# github.com/biogo/hts v1.0.1
github.com/biogo/hts/bam
github.com/biogo/hts/bgzf
github.com/biogo/hts/bgzf/index
github.com/biogo/hts/internal
github.com/biogo/hts/sam
# github.com/gin-contrib/sse v0.1.0
github.com/gin-contrib/sse
# github.com/gin-contrib/static v0.0.0-20191128031702-f81c604d8ac2
github.com/gin-contrib/static
# github.com/gin-gonic/gin v1.5.0
github.com/gin-gonic/gin
github.com/gin-gonic/gin/binding
github.com/gin-gonic/gin/internal/json
github.com/gin-gonic/gin/render
# github.com/go-playground/locales v0.13.0
github.com/go-playground/locales
github.com/go-playground/locales/currency
# github.com/go-playground/universal-translator v0.17.0
github.com/go-playground/universal-translator
# github.com/golang/protobuf v1.3.2
github.com/golang/protobuf/proto
# github.com/inconshreveable/mousetrap v1.0.0
github.com/inconshreveable/mousetrap
# github.com/json-iterator/go v1.1.8
github.com/json-iterator/go
# github.com/leodido/go-urn v1.2.0
github.com/leodido/go-urn
# github.com/mattn/go-isatty v0.0.11
github.com/mattn/go-isatty
# github.com/mattn/go-runewidth v0.0.4
github.com/mattn/go-runewidth
# github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd
github.com/modern-go/concurrent
# github.com/modern-go/reflect2 v1.0.1
github.com/modern-go/reflect2
# github.com/olekukonko/tablewriter v0.0.2
github.com/olekukonko/tablewriter
# github.com/sakkayaphab/genie/bed v0.0.0 => ./bed
github.com/sakkayaphab/genie/bed
# github.com/sakkayaphab/genie/cli v0.0.0 => ./cli
github.com/sakkayaphab/genie/cli
# github.com/sakkayaphab/genie/execute v0.0.0 => ./execute
github.com/sakkayaphab/genie/execute
# github.com/sakkayaphab/genie/fasta v0.0.0 => ./fasta
github.com/sakkayaphab/genie/fasta
# github.com/sakkayaphab/genie/fastq v0.0.0 => ./fastq
github.com/sakkayaphab/genie/fastq
# github.com/sakkayaphab/genie/gff v0.0.0 => ./gff
github.com/sakkayaphab/genie/gff
# github.com/sakkayaphab/genie/hts v0.0.0 => ./hts
github.com/sakkayaphab/genie/hts
# github.com/sakkayaphab/genie/variant v0.0.0 => ./variant
github.com/sakkayaphab/genie/variant
# github.com/sakkayaphab/genie/vcf v0.0.0 => ./vcf
github.com/sakkayaphab/genie/vcf
# github.com/sakkayaphab/genie/web v0.0.0 => ./web
github.com/sakkayaphab/genie/web
# github.com/spf13/cobra v0.0.5
github.com/spf13/cobra
# github.com/spf13/pflag v1.0.3
github.com/spf13/pflag
# github.com/ugorji/go/codec v1.1.7
github.com/ugorji/go/codec
# golang.org/x/sys v0.0.0-20191218084908-4a24b4065292
golang.org/x/sys/unix
# gopkg.in/go-playground/validator.v9 v9.30.2
gopkg.in/go-playground/validator.v9
# gopkg.in/yaml.v2 v2.2.7
gopkg.in/yaml.v2
