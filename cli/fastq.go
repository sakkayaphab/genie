package cli

import (
	"fmt"
	"github.com/spf13/cobra"
	"log"
	"os"
	"github.com/sakkayaphab/fastqgo"
)

var fastqCmd = &cobra.Command{
	Use:   "fastq",
	Short: "manage FASTQ file",
	Run: func(cmd *cobra.Command, args []string) {
		cmd.Help()
	},
}

var fastqCountCmd = &cobra.Command{
	Use:   "count",
	Short: "count a FASTQ file",
	Run: func(cmd *cobra.Command, args []string) {
		filepath, err := cmd.Flags().GetString("filepath")
		if err != nil {
			log.Fatal(err)
		}
		if filepath=="" {
			cmd.Help()
			os.Exit(0)
		}

		fqReader := fastqgo.GetFastQReader()
		fqReader.FilePath = filepath
		count,err := fqReader.Count()
		if err!=nil {
			log.Fatal(err)
		}

		fmt.Println(count)
	},
}


var fastqSplitCmd = &cobra.Command{
	Use:   "split",
	Short: "split a single FASTQ file to pair FASTQ files (paired-end reads)",
	Run: func(cmd *cobra.Command, args []string) {
		filepath, err := cmd.Flags().GetString("filepath")
		if err != nil {
			log.Fatal(err)
		}
		if filepath=="" {
			cmd.Help()
			os.Exit(0)
		}

		fq, err := cmd.Flags().GetString("fq")
		if err != nil {
			log.Fatal(err)
		}
		if fq=="" {
			cmd.Help()
			os.Exit(0)
		}

		fq2, err := cmd.Flags().GetString("fq2")
		if err != nil {
			log.Fatal(err)
		}
		if fq2=="" {
			cmd.Help()
			os.Exit(0)
		}

		fqReader := fastqgo.GetFastQReader()
		fqReader.FilePath = filepath
		count,err := fqReader.SplitFastqFromSingleFastq(fq,fq2)
		if err!=nil {
			log.Fatal(err)
		}

		fmt.Println(count)
	},
}