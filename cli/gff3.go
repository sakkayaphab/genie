package cli

import (
	"github.com/sakkayaphab/genie/gff"
	"github.com/spf13/cobra"
	"log"
	"os"
)

var gff3Cmd = &cobra.Command{
	Use:   "gff3",
	Short: "manage GFF3 file",
	Run: func(cmd *cobra.Command, args []string) {
		cmd.Help()
	},
}

var gff3ConvertCmd = &cobra.Command{
	Use:   "convert",
	Short: "convert gff3 to other formats such as TSV and CSV",
	Run: func(cmd *cobra.Command, args []string) {
		csvconfig := gff.NewCsvConfig()
		filepath, err := cmd.Flags().GetString("filepath")
		if err != nil {
			log.Fatal(err)
		}
		if filepath=="" {
			cmd.Help()
			os.Exit(0)
		}
		csvconfig.FilePath = filepath

		seqid, err := cmd.Flags().GetBool("seqid")
		if err != nil {
			log.Fatal(err)
		}
		csvconfig.SeqID = seqid
		source, err := cmd.Flags().GetBool("source")
		if err != nil {
			log.Fatal(err)
		}
		csvconfig.Source = source

		typeData, err := cmd.Flags().GetBool("type")
		if err != nil {
			log.Fatal(err)
		}
		csvconfig.TypeData = typeData

		start, err := cmd.Flags().GetBool("start")
		if err != nil {
			log.Fatal(err)
		}
		csvconfig.Start = start

		end, err := cmd.Flags().GetBool("end")
		if err != nil {
			log.Fatal(err)
		}
		csvconfig.End = end

		score, err := cmd.Flags().GetBool("score")
		if err != nil {
			log.Fatal(err)
		}
		csvconfig.Score = score

		strand, err := cmd.Flags().GetBool("strand")
		if err != nil {
			log.Fatal(err)
		}
		csvconfig.Strand = strand

		phase, err := cmd.Flags().GetBool("phase")
		if err != nil {
			log.Fatal(err)
		}
		csvconfig.Phase = phase

		attributes, err := cmd.Flags().GetStringArray("attributes")
		if err != nil {
			log.Fatal(err)
		}
		csvconfig.Attributes = attributes

		header, err := cmd.Flags().GetBool("header")
		if err != nil {
			log.Fatal(err)
		}

		limit, err := cmd.Flags().GetInt64("limit")
		if err != nil {
			log.Fatal(err)
		}
		csvconfig.Limit = limit

		separator, err := cmd.Flags().GetString("separator")
		if err != nil {
			log.Fatal(err)
		}
		if len(separator)==2 && separator[0:1]=="\\" {
			if separator[1:]=="t" {
				separator = "\t"
			} else if separator[1:]=="n" {
				separator = "\n"
			} else if separator[1:]=="v" {
				separator = "\v"
			} else {
				log.Fatal("cannot parse --separator")
			}
		}
		csvconfig.Separator = separator

		replacement, err := cmd.Flags().GetString("replacement")
		if err != nil {
			log.Fatal(err)
		}
		csvconfig.Replacement = replacement

		if !header {
			csvconfig.ShowHeader(csvconfig.Replacement,csvconfig.Separator)
		}

		err = csvconfig.ConvertToCSV()
		if err!=nil {
			log.Fatal(err)
		}
	},
}