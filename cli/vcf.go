package cli

import (
	"github.com/spf13/cobra"
	"log"
	"os"
	"github.com/sakkayaphab/genie/variant"
)

var variantVCFCmd = &cobra.Command{
	Use:   "vcf",
	Short: "manage VCF file",
	Run: func(cmd *cobra.Command, args []string) {
		cmd.Help()
	},
}

var variantCompareVcfAndVcfCmd = &cobra.Command{
	Use:   "comparetovcf",
	Short: "compare between VCF and VCF files",
	Run: func(cmd *cobra.Command, args []string) {

		filepaths, err := cmd.Flags().GetStringArray("filepath")
		if err != nil {
			log.Fatal(err)
		}

		svtype, err := cmd.Flags().GetString("svtype")
		if err != nil {
			log.Fatal(err)
		}

		reciprocal, err := cmd.Flags().GetInt("reciprocal")
		if err != nil {
			log.Fatal(err)
		}

		if reciprocal < 10 && reciprocal > 100 {
			log.Fatalln("reciprocal >= 10 and reciprocal =< 100")
		}

		overlap, err := cmd.Flags().GetInt("overlap")
		if err != nil {
			log.Fatal(err)
		}

		if overlap < 0 {
			log.Fatalln("overlap >= 0")
		}

		if len(filepaths) == 0 {
			cmd.Help()
			os.Exit(0)
		}

		if len(filepaths) <= 1 {
			cmd.Help()
			os.Exit(0)
		}

		outputpl, err := cmd.Flags().GetString("outputpl")
		if err != nil {
			log.Fatal(err)
		}

		outputpu, err := cmd.Flags().GetString("outputpu")
		if err != nil {
			log.Fatal(err)
		}

		outputsl, err := cmd.Flags().GetString("outputsl")
		if err != nil {
			log.Fatal(err)
		}

		outputsu, err := cmd.Flags().GetString("outputsu")
		if err != nil {
			log.Fatal(err)
		}

		for i, j := 0, len(filepaths)-1; i < j; i, j = i+1, j-1 {
			filepaths[i], filepaths[j] = filepaths[j], filepaths[i]
		}

		variant.CompareVcfAndVcf(filepaths, svtype, reciprocal, overlap, outputpl, outputpu, outputsl, outputsu)
	},
}


var variantCompareBedAndVcfCmd = &cobra.Command{
	Use:   "comparetobed",
	Short: "compare between BED and VCF files",
	Run: func(cmd *cobra.Command, args []string) {

		filepaths, err := cmd.Flags().GetStringArray("filepath")
		if err != nil {
			log.Fatal(err)
		}

		svtype, err := cmd.Flags().GetString("svtype")
		if err != nil {
			log.Fatal(err)
		}

		reciprocal, err := cmd.Flags().GetInt("reciprocal")
		if err != nil {
			log.Fatal(err)
		}

		if reciprocal < 10 && reciprocal > 100 {
			log.Fatalln("reciprocal >= 10 and reciprocal =< 100")
		}

		overlap, err := cmd.Flags().GetInt("overlap")
		if err != nil {
			log.Fatal(err)
		}

		if overlap < 0 {
			log.Fatalln("overlap >= 0")
		}

		if len(filepaths) == 0 {
			cmd.Help()
			os.Exit(0)
		}

		if len(filepaths) <= 1 {
			cmd.Help()
			os.Exit(0)
		}

		for i, j := 0, len(filepaths)-1; i < j; i, j = i+1, j-1 {
			filepaths[i], filepaths[j] = filepaths[j], filepaths[i]
		}

		variant.CompareBedAndVCF(filepaths, svtype, reciprocal, overlap)

	},
}


var variantVcfFilterCmd = &cobra.Command{
	Use:   "filter",
	Short: "filter VCF file",
	Run: func(cmd *cobra.Command, args []string) {
		filepath, err := cmd.Flags().GetString("filepath")
		if err != nil {
			cmd.Help()
			os.Exit(0)
		}

		if filepath == "" {
			cmd.Help()
			os.Exit(0)
		}

		output, err := cmd.Flags().GetString("output")
		if err != nil {
			cmd.Help()
			os.Exit(0)
		}

		onlysv, err := cmd.Flags().GetBool("onlysv")
		if err != nil {
			cmd.Help()
			os.Exit(0)
		}

		minlength, err := cmd.Flags().GetInt("minlength")
		if err != nil {
			cmd.Help()
			os.Exit(0)
		}

		maxlength, err := cmd.Flags().GetInt("maxlength")
		if err != nil {
			cmd.Help()
			os.Exit(0)
		}

		var vm variant.VcfManagement
		vm.OnlySv = onlysv
		vm.MinSvLength = minlength
		vm.MaxSvLength = maxlength

		vm.ManageVCF(filepath, output)
	},
}

var variantVcfStatsCmd = &cobra.Command{
	Use:   "stats",
	Short: "view statistics of a VCF file",
	Run: func(cmd *cobra.Command, args []string) {
		filepath, err := cmd.Flags().GetString("filepath")
		if err != nil {
			cmd.Help()
			os.Exit(0)
		}

		if filepath == "" {
			cmd.Help()
			os.Exit(0)
		}

		variant.ViewStatsVCF(filepath)
	},
}

var variantVennCmd = &cobra.Command{
	Use:   "venn",
	Short: "Show a Venn diagram (this version support only three sets)",
	Run: func(cmd *cobra.Command, args []string) {

		filepaths, err := cmd.Flags().GetStringArray("filepath")
		if err != nil {
			log.Fatal(err)
		}

		if len(filepaths) != 3 {
			cmd.Help()
			os.Exit(0)
		}

		svtype, err := cmd.Flags().GetString("svtype")
		if err != nil {
			cmd.Help()
			os.Exit(0)
		}

		if svtype == "" {
			cmd.Help()
			os.Exit(0)
		}

		// primary, err := cmd.Flags().GetString("primary")
		// if err != nil {
		// 	cmd.Help()
		// 	os.Exit(0)
		// }

		// if primary == "" {
		// 	cmd.Help()
		// 	os.Exit(0)
		// }

		variant.CreateVennDiagram(filepaths[0], filepaths[1], filepaths[2], svtype)

	},
}

var variantCsvCmd = &cobra.Command{
	Use:   "csv",
	Short: "parse pos and end to csv",
	Run: func(cmd *cobra.Command, args []string) {

		//filepaths, err := cmd.Flags().GetString("filepath")
		//if err != nil {
		//	log.Fatal(err)
		//}
		//variant.CreateVennDiagram(filepaths[0], filepaths[1], filepaths[2], svtype)
	},
}