package cli

import (
	"fmt"
	"github.com/sakkayaphab/genie/bed"
	"github.com/spf13/cobra"
	"log"
	"os"
)

var bedCmd = &cobra.Command{
	Use:   "bed",
	Short: "manage BED file",
	Run: func(cmd *cobra.Command, args []string) {
		cmd.Help()
	},
}

var bedViewCmd = &cobra.Command{
	Use:   "view",
	Short: "view a BED file",
	Run: func(cmd *cobra.Command, args []string) {
		filepath, err := cmd.Flags().GetString("filepath")
		if err != nil {
			cmd.Help()
			os.Exit(0)
		}
		if filepath == "" {
			cmd.Help()
			os.Exit(0)
		}

		bedviewer := bed.NewViewer()
		bedviewer.FilePath = filepath

		err = bedviewer.ViewBED()
		if err!=nil {
			log.Fatal(err)
		}

	},
}

var bedCountCmd = &cobra.Command{
	Use:   "count",
	Short: "count a BED file",
	Run: func(cmd *cobra.Command, args []string) {
		filepath, err := cmd.Flags().GetString("filepath")
		if err != nil {
			cmd.Help()
			os.Exit(0)
		}
		if filepath == "" {
			cmd.Help()
			os.Exit(0)
		}

		bedviewer := bed.NewViewer()
		bedviewer.FilePath = filepath

		count,err := bedviewer.CountBED()
		if err!=nil {
			log.Fatal(err)
		}

		fmt.Println(count)
	},
}