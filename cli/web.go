package cli

import (
	"github.com/sakkayaphab/genie/account"
	"fmt"
	"github.com/sakkayaphab/genie/web"
	"github.com/spf13/cobra"
	"log"
	"os"
)

var webCmd = &cobra.Command{
	Use:   "web",
	Short: "manage web base application",
	Run: func(cmd *cobra.Command, args []string) {
		cmd.Help()
	},
}

var webInitConfigCmd = &cobra.Command {
	Use:   "init",
	Short: "initialize for web base application",
	Run: func(cmd *cobra.Command, args []string) {
		web.WriteDBConfig()

		cfg,err := web.ReadConfig()
		if err!=nil {
			log.Fatal(err)
		}

		cfgPretty,err := cfg.ToPrettyJson()
		if err!=nil {
			log.Fatal(err)
		}

		fmt.Println(cfgPretty)
	},
}

var webAddUserCmd = &cobra.Command{
	Use:   "adduser",
	Short: "add user for web base application",
	Run: func(cmd *cobra.Command, args []string) {
		cfg,err := web.ReadConfig()
		if err != nil {
			log.Fatal( err )
		}

		acfg,err := cfg.GenarateAccountConfig()
		if err != nil {
			log.Fatal( err )
		}

		username, err := cmd.Flags().GetString("username")
		if err != nil {
			cmd.Help()
			os.Exit(0)
		}
		if username == "" {
			cmd.Help()
			os.Exit(0)
		}

		password, err := cmd.Flags().GetString("password")
		if err != nil {
			cmd.Help()
			os.Exit(0)
		}
		if password == "" {
			cmd.Help()
			os.Exit(0)
		}


		profile,err := acfg.AddProfileByUsernamePassword(username,account.GenerateSHA512HMACHash(password))
		if err != nil {
			log.Fatal( err )
		}

		profilePretty,err := profile.ToPrettyJson()
		if err != nil {
			log.Fatal( err )
		}
		fmt.Println(profilePretty)
	},
}

var webStartCmd = &cobra.Command{
	Use:   "start",
	Short: "start web base application",
	Run: func(cmd *cobra.Command, args []string) {
		web.Start()
	},
}

var webProfilesCmd = &cobra.Command{
	Use:   "profiles",
	Short: "show all profile",
	Run: func(cmd *cobra.Command, args []string) {
		cfg,err := web.ReadConfig()
		if err != nil {
			log.Fatal( err )
		}

		acfg,err := cfg.GenarateAccountConfig()
		if err != nil {
			log.Fatal( err )
		}

		profiles,err := acfg.GetAllProfile()
		if err != nil {
			log.Fatal( err )
		}

		for _,x := range *profiles {
			text,err := x.ToPrettyJson()
			if err != nil {
				log.Fatal( err )
			}
			fmt.Println(text)
		}
	},
}

var webTokensCmd = &cobra.Command{
	Use:   "tokens",
	Short: "show all token",
	Run: func(cmd *cobra.Command, args []string) {
		cfg,err := web.ReadConfig()
		if err != nil {
			log.Fatal( err )
		}

		acfg,err := cfg.GenarateAccountConfig()
		if err != nil {
			log.Fatal( err )
		}

		profiles,err := acfg.GetAllToken()
		if err != nil {
			log.Fatal( err )
		}

		for _,x := range *profiles {
			text,err := x.ToPrettyJson()
			if err != nil {
				log.Fatal( err )
			}
			fmt.Println(text)
		}
	},
}

