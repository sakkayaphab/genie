package cli

import (
	"fmt"
	"github.com/spf13/cobra"
	"log"
	"os"
	"github.com/sakkayaphab/fastago"
)

var fastaCmd = &cobra.Command{
	Use:   "fasta",
	Short: "manage FASTA file",
	Run: func(cmd *cobra.Command, args []string) {
		cmd.Help()
	},
}

var fastaViewCmd = &cobra.Command{
	Use:   "view",
	Short: "view a FASTA file",
	Run: func(cmd *cobra.Command, args []string) {
		filepath, err := cmd.Flags().GetString("filepath")
		if err != nil {
			cmd.Help()
			os.Exit(0)
		}
		if filepath == "" {
			cmd.Help()
			os.Exit(0)
		}

		reader, err := fastago.New(filepath)
		if err != nil {
			panic(err)
		}
		err = reader.ReadIndex()
		if err != nil {
			panic(err)
		}

		scopeCmd, err := cmd.Flags().GetString("scope")
		if err != nil {
			log.Fatal(err)
		}

		if scopeCmd=="" {
			cmd.Help()
			os.Exit(0)
		}

		scope,err := reader.ParseStringToScope(scopeCmd)
		if err!=nil {
			log.Println(err)
		}

		if scope.OnlyChr {
			log.Println(err)
		}

		if scope.Begin<0 {
			log.Println(err)
		}

		if scope.End<0 {
			log.Println(err)
		}

		if scope.End<scope.Begin {
			log.Println(err)
		}

		seq,err := reader.GetSequence(scope.Chr,scope.Begin,scope.End)
		if err!=nil {
			log.Fatal(err)
		}

		fmt.Println(*seq)
	},
}



var fastaFaidxCmd = &cobra.Command{
	Use:   "faidx",
	Short: "generate faidx",
	Run: func(cmd *cobra.Command, args []string) {
		filepath, err := cmd.Flags().GetString("filepath")
		if err != nil {
			cmd.Help()
			os.Exit(0)
		}
		if filepath == "" {
			cmd.Help()
			os.Exit(0)
		}

		reader, err := fastago.New(filepath)
		if err != nil {
			panic(err)
		}
		err = reader.DeleteFaidx()
		if err != nil {
			panic(err)
		}

		err = reader.ReadIndex()
		if err != nil {
			panic(err)
		}

	},
}
