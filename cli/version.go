package cli

import (
	"fmt"
	"github.com/spf13/cobra"
)

var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "displays the current Genie version",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("genie 0.8.0")
	},
}
