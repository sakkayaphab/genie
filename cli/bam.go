package cli

import (
	"fmt"
	"github.com/spf13/cobra"
	"github.com/sakkayaphab/genie/hts"
	"log"
	"os"
)

var bamCmd = &cobra.Command{
	Use:   "bam",
	Short: "manage BAM file",
	Run: func(cmd *cobra.Command, args []string) {
		cmd.Help()
	},
}

var bamViewCmd = &cobra.Command{
	Use:   "view",
	Short: "view a BAM file",
	Run: func(cmd *cobra.Command, args []string) {
		scopeCmdLists, err := cmd.Flags().GetStringArray("scope")
		if err != nil {
			log.Fatal(err)
		}

		filepath, err := cmd.Flags().GetString("filepath")
		if err != nil {
			cmd.Help()
			os.Exit(0)
		}
		if filepath == "" {
			cmd.Help()
			os.Exit(0)
		}

		headeronly, err := cmd.Flags().GetBool("headeronly")
		if err != nil {
			cmd.Help()
			os.Exit(0)
		}

		includeheader, err := cmd.Flags().GetBool("includeheader")
		if err != nil {
			cmd.Help()
			os.Exit(0)
		}

		limitreads, err := cmd.Flags().GetInt("limitreads")
		if err != nil {
			//log.Fatal(err)
			cmd.Help()
			os.Exit(0)
		}

		humanreadable, err := cmd.Flags().GetBool("human-readable")
		if err != nil {
			//log.Fatal(err)
			cmd.Help()
			os.Exit(0)
		}

		var scopes []hts.ScopeRead
		if len(scopeCmdLists)!=0 {
			for _,x := range scopeCmdLists {
				scope, err := hts.ParseStringToScope(x)
				if err != nil {
					log.Fatal(err)
				}
				scopes = append(scopes, scope)
			}
		}

		err = hts.ReadfileBam(filepath, headeronly, includeheader, scopes,limitreads,humanreadable)
		if err!=nil {
			log.Fatal(err)
		}
	},
}


var bamToFastqCmd = &cobra.Command{
	Use:   "tofastq",
	Short: "convert BAM to FASTQ",
	Run: func(cmd *cobra.Command, args []string) {

		bam, err := cmd.Flags().GetString("bam")
		if err != nil {
			cmd.Help()
			os.Exit(0)
		}
		if bam == "" {
			cmd.Help()
			os.Exit(0)
		}

		fq, err := cmd.Flags().GetString("fq")
		if err != nil {
			cmd.Help()
			os.Exit(0)
		}
		if fq == "" {
			cmd.Help()
			os.Exit(0)
		}

		fq2, err := cmd.Flags().GetString("fq2")
		if err != nil {
			cmd.Help()
			os.Exit(0)
		}

		requiredflags, err := cmd.Flags().GetUint16("requiredflags")
		if err != nil {
			cmd.Help()
			os.Exit(0)
		}

		excludedflags, err := cmd.Flags().GetUint16("excludedflags")
		if err != nil {
			cmd.Help()
			os.Exit(0)
		}

		_,err = hts.ConvertBamToFastq(bam,fq,fq2,requiredflags,excludedflags)
		if err!=nil {
			log.Fatal(err)
		}

	},
}

var bamCountCmd = &cobra.Command{
	Use:   "count",
	Short: "count records",
	Run: func(cmd *cobra.Command, args []string) {

		bam, err := cmd.Flags().GetString("bam")
		if err != nil {
			cmd.Help()
			os.Exit(0)
		}
		if bam == "" {
			cmd.Help()
			os.Exit(0)
		}

		requiredflags, err := cmd.Flags().GetUint16("requiredflags")
		if err != nil {
			cmd.Help()
			os.Exit(0)
		}

		excludedflags, err := cmd.Flags().GetUint16("excludedflags")
		if err != nil {
			cmd.Help()
			os.Exit(0)
		}

		count,err := hts.CountBam(bam,requiredflags,excludedflags)
		if err != nil {
			log.Fatal(err)
		}

		fmt.Println(count)
	},
}


var bamToSamCmd = &cobra.Command{
	Use:   "tosam",
	Short: "convert BAM to SAM",
	Run: func(cmd *cobra.Command, args []string) {

		bam, err := cmd.Flags().GetString("bam")
		if err != nil {
			cmd.Help()
			os.Exit(0)
		}
		if bam == "" {
			cmd.Help()
			os.Exit(0)
		}

		sam, err := cmd.Flags().GetString("sam")
		if err != nil {
			cmd.Help()
			os.Exit(0)
		}
		if sam == "" {
			cmd.Help()
			os.Exit(0)
		}

		requiredflags, err := cmd.Flags().GetUint16("requiredflags")
		if err != nil {
			cmd.Help()
			os.Exit(0)
		}

		excludedflags, err := cmd.Flags().GetUint16("excludedflags")
		if err != nil {
			cmd.Help()
			os.Exit(0)
		}

		_,err = hts.ConvertBamToSam(bam,sam,requiredflags,excludedflags)
		if err!=nil {
			log.Fatal(err)
		}

	},
}