package cli

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use:   "genie",
	Short: "A toolkit for working with next-generation sequencing data",
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 0 {
			cmd.Help()
			os.Exit(0)
		}
	},
}

var helpCmd = &cobra.Command{
	Use:   "help",
	Short: "help about any command",
	Long: "Need help? Go to the https://github.com/sakkayaphab/genie",
	Run: func(cmd *cobra.Command, args []string) {
		cmd.Help()
	},
}

func Execute() {
	rootCmd.AddCommand(versionCmd)
	rootCmd.SetHelpCommand(helpCmd)
	rootCmd.AddCommand(webCmd)
	webCmd.AddCommand(webAddUserCmd)
	webAddUserCmd.Flags().StringP("username", "u", "", "username (*require)")
	webAddUserCmd.Flags().StringP("password", "p", "", "username (*require)")
	webCmd.AddCommand(webInitConfigCmd)
	webCmd.AddCommand(webStartCmd)
	webCmd.AddCommand(webProfilesCmd)
	webCmd.AddCommand(webTokensCmd)

	addCompareVariantCommand(rootCmd)
	addBamCommand(rootCmd)
	addFastaCommand(rootCmd)
	addGffCommand(rootCmd)
	addFastQCommand(rootCmd)
	addBedCommand(rootCmd)
	addSamCommand(rootCmd)


	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func addBedCommand(rootCmd *cobra.Command) {
	rootCmd.AddCommand(bedCmd)
	bedCmd.AddCommand(bedViewCmd)
	bedViewCmd.Flags().StringP("filepath", "f", "", "A bam file (*require)")

	bedCmd.AddCommand(bedCountCmd)
	bedCountCmd.Flags().StringP("filepath", "f", "", "A bam file (*require)")
}

func addBamCommand(rootCmd *cobra.Command) {
	rootCmd.AddCommand(bamCmd)
	bamCmd.AddCommand(bamViewCmd)
	bamViewCmd.Flags().StringP("filepath", "f", "", "A bam file (*require)")
	bamViewCmd.Flags().BoolP("headeronly", "H", false, "Show the header only")
	bamViewCmd.Flags().BoolP("includeheader", "I", false, "Show header and records")
	bamViewCmd.Flags().StringArrayP("scope", "s", nil, "Scope chr:begin-end")
	bamViewCmd.Flags().Int("limitreads", -1, "Limit reads (-1 is unlimited)")
	bamViewCmd.Flags().Bool("human-readable", false, "Show reads in human readable format")

	bamCmd.AddCommand(bamToFastqCmd)
	bamToFastqCmd.Flags().StringP("bam", "b", "", "Input BAM file (*require)")
	bamToFastqCmd.Flags().String("fq",  "", "Output FASTQ file (fq1 for paired-end reads) (*require)")
	bamToFastqCmd.Flags().String("fq2", "", "Output FASTQ2 for paired-end reads (If empty, all data will be at fq)")
	bamToFastqCmd.Flags().Uint16P("requiredflags", "f",0, "required flags (int16)")
	bamToFastqCmd.Flags().Uint16P("excludedflags", "F",0, "excluded flags (int16)")

	bamCmd.AddCommand(bamToSamCmd)
	bamToSamCmd.Flags().StringP("bam", "b", "", "Input BAM file (*require)")
	bamToSamCmd.Flags().StringP("sam",  "s","", "Output SAM file (*require)")
	bamToSamCmd.Flags().Uint16P("requiredflags", "f",0, "required flags (int16)")
	bamToSamCmd.Flags().Uint16P("excludedflags", "F",0, "excluded flags (int16)")

	bamCmd.AddCommand(bamCountCmd)
	bamCountCmd.Flags().StringP("bam", "b", "", "Input BAM file (*require)")
	bamCountCmd.Flags().Uint16P("requiredflags", "f",0, "required flags (uint16)")
	bamCountCmd.Flags().Uint16P("excludedflags", "F",0, "excluded flags (uint16)")
}

func addSamCommand(rootCmd *cobra.Command) {
	rootCmd.AddCommand(samCmd)
	samCmd.AddCommand(samToFastqCmd)
	samToFastqCmd.Flags().StringP("sam", "s", "", "Input SAM file (*require)")
	samToFastqCmd.Flags().String("fq",  "", "Output FASTQ file (fq1 for paired-end reads) (*require)")
	samToFastqCmd.Flags().String("fq2", "", "Output FASTQ2 for paired-end reads (If empty, all data will be at fq)")
	samToFastqCmd.Flags().Uint16P("requiredflags", "f",0, "required flags (int16)")
	samToFastqCmd.Flags().Uint16P("excludedflags", "F",0, "excluded flags (int16)")

	samCmd.AddCommand(samToBamCmd)
	samToBamCmd.Flags().StringP("sam", "s", "", "Input SAM file (*require)")
	samToBamCmd.Flags().StringP("bam", "b", "", "Output BAM file (*require)")

	samCmd.AddCommand(samCountCmd)
	samCountCmd.Flags().StringP("sam", "s", "", "Input SAM file (*require)")
	samCountCmd.Flags().Uint16P("requiredflags", "f",0, "required flags (uint16)")
	samCountCmd.Flags().Uint16P("excludedflags", "F",0, "excluded flags (uint16)")

	samCmd.AddCommand(samViewCmd)
	samViewCmd.Flags().StringP("filepath", "f", "", "A bam file (*require)")
	samViewCmd.Flags().BoolP("headeronly", "H", false, "Show the header only")
	samViewCmd.Flags().BoolP("includeheader", "I", false, "Show header and records")
	samViewCmd.Flags().StringArrayP("scope", "s", nil, "Scope chr:begin-end")
	samViewCmd.Flags().Int64("limitreads", -1, "Limit reads (-1 is unlimited)")
	samViewCmd.Flags().Bool("human-readable", false, "Show reads in human readable format")
}
func addCompareVariantCommand(rootCmd *cobra.Command)  {
	rootCmd.AddCommand(variantVCFCmd)
	variantVCFCmd.AddCommand(variantCompareVcfAndVcfCmd)
	variantCompareVcfAndVcfCmd.Flags().StringArrayP("filepath", "f", nil, "vcf files (*require 2 files, first file must be primary file)")
	variantCompareVcfAndVcfCmd.Flags().StringP("svtype", "t", "", "compare only SVTYPE such as DEL, DUP, INV, INS and BND")
	variantCompareVcfAndVcfCmd.Flags().IntP("reciprocal", "r", 90, "The percentage of reciprocal overlap is used only in the case of DEL, DUP, INV")
	variantCompareVcfAndVcfCmd.Flags().IntP("overlap", "o", 300, "The bases of overlap is used only in the case of INS, BND")
	variantCompareVcfAndVcfCmd.Flags().StringP("outputpl", "p","", "output of primary file that overlapped")
	variantCompareVcfAndVcfCmd.Flags().StringP("outputpu", "u","", "output of primary file that unoverlapped")
	variantCompareVcfAndVcfCmd.Flags().StringP("outputsl", "s","", "output of secondary file that overlapped")
	variantCompareVcfAndVcfCmd.Flags().StringP("outputsu", "l","", "output of secondary file that unoverlapped")


	variantVCFCmd.AddCommand(variantCompareBedAndVcfCmd)
	variantCompareBedAndVcfCmd.Flags().StringP("svtype", "t", "", "(*require) compare only SVTYPE such as DEL, DUP, INV, INS and BND")
	variantCompareBedAndVcfCmd.Flags().StringArrayP("filepath", "f", nil, "vcf files (*require 2 files, first file must be primary file)")
	variantCompareBedAndVcfCmd.Flags().IntP("reciprocal", "r", 90, "The percentage of reciprocal overlap is used only in the case of DEL, DUP, INV")
	variantCompareBedAndVcfCmd.Flags().IntP("overlap", "o", 300, "The bases of overlap is used only in the case of INS, BND")

	variantVCFCmd.AddCommand(variantVcfFilterCmd)
	variantVcfFilterCmd.Flags().StringP("filepath", "f", "", "a VCF file (*require)")
	variantVcfFilterCmd.Flags().StringP("output", "o", "", "output file")
	variantVcfFilterCmd.Flags().IntP("minlength", "n", -2147483647, "Min SVLENGTH")
	variantVcfFilterCmd.Flags().IntP("maxlength", "m", 2147483647, "Max SVLENGTH")
	variantVcfFilterCmd.Flags().BoolP("onlysv", "y", false, "Only SVs")

	variantVCFCmd.AddCommand(variantVcfStatsCmd)
	variantVcfStatsCmd.Flags().StringP("filepath", "f", "", "a VCF file (*require)")

	variantVCFCmd.AddCommand(variantVennCmd)
	variantVennCmd.Flags().StringP("primary", "p", "", "a primary VCF file (*require)")
	variantVennCmd.Flags().StringArrayP("filepath", "f", nil, "VCF files (*require at 3 files)")
	variantVennCmd.Flags().StringP("svtype", "t", "", "SVTYPE such as DEL, DUP, INV, INS and BND (*require, support only one type)")
}

func addFastaCommand(rootCmd *cobra.Command)  {
	rootCmd.AddCommand(fastaCmd)
	fastaCmd.AddCommand(fastaViewCmd)
	fastaViewCmd.Flags().StringP("filepath", "f", "", "A bam file (*require)")
	fastaViewCmd.Flags().StringP("scope", "s", "", "Scope chr:begin-end (*require)")
	fastaCmd.AddCommand(fastaFaidxCmd)
	fastaFaidxCmd.Flags().StringP("filepath", "f", "", "A bam file (*require)")
}

func addGffCommand(rootCmd *cobra.Command)  {
	rootCmd.AddCommand(gff3Cmd)

	gff3Cmd.AddCommand(gff3ConvertCmd)
	gff3ConvertCmd.Flags().StringP("filepath", "f", "", "A bam file (*require)")
	gff3ConvertCmd.Flags().Bool("seqid", false, "select seqid")
	gff3ConvertCmd.Flags().Bool("source", false, "select source")
	gff3ConvertCmd.Flags().Bool("type", false, "select type")
	gff3ConvertCmd.Flags().Bool("start", false, "select start")
	gff3ConvertCmd.Flags().Bool("end", false, "select end")
	gff3ConvertCmd.Flags().Bool("score", false, "select score")
	gff3ConvertCmd.Flags().Bool("strand", false, "select strand")
	gff3ConvertCmd.Flags().Bool("phase", false, "select phase")
	gff3ConvertCmd.Flags().StringArray("attributes", nil, "select attributes by key")
	gff3ConvertCmd.Flags().Bool("header", false, "hiding headers")
	gff3ConvertCmd.Flags().Int64("limit",-1, "Output Limit")
	gff3ConvertCmd.Flags().String("replacement", "", "the replacement character if there is a blank value")
	gff3ConvertCmd.Flags().String("separator", "\t", "the separator of each values")
}

func addFastQCommand(rootCmd *cobra.Command)  {
	rootCmd.AddCommand(fastqCmd)

	fastqCmd.AddCommand(fastqCountCmd)
	fastqCountCmd.Flags().StringP("filepath", "f", "", "Input FASTQ file (*require)")

	fastqCmd.AddCommand(fastqSplitCmd)
	fastqSplitCmd.Flags().StringP("filepath", "f", "", "Input FASTQ file (*require)")
	fastqSplitCmd.Flags().String("fq",  "", "Output FASTQ1 file (*require)")
	fastqSplitCmd.Flags().String("fq2", "", "Output FASTQ2 file (*require)")
}
