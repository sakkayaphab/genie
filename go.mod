module github.com/sakkayaphab/genie

go 1.13

require (
	github.com/biogo/boom v0.0.0-20150317015657-28119bc1ffc1 // indirect
	github.com/sakkayaphab/genie/cli v0.0.0
	github.com/sakkayaphab/vcfgo v0.1.0 // indirect
)

replace (
	github.com/sakkayaphab/genie/account v0.0.0 => ./account
	github.com/sakkayaphab/genie/bed v0.0.0 => ./bed
	github.com/sakkayaphab/genie/cli v0.0.0 => ./cli
	github.com/sakkayaphab/genie/execute v0.0.0 => ./execute
	github.com/sakkayaphab/genie/fasta v0.0.0 => ./fasta
	github.com/sakkayaphab/genie/filemanager v0.0.0 => ./filemanager
	github.com/sakkayaphab/genie/gff v0.0.0 => ./gff
	github.com/sakkayaphab/genie/gridengine v0.0.0 => ./gridengine
	github.com/sakkayaphab/genie/hts v0.0.0 => ./hts
	github.com/sakkayaphab/genie/variant v0.0.0 => ./variant
	github.com/sakkayaphab/genie/web v0.0.0 => ./web
)
