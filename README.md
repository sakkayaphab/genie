# Genie

![Build Status](https://github.com/sakkayaphab/genie/workflows/Windows/badge.svg?branch=master)
![Build Status](https://github.com/sakkayaphab/genie/workflows/Ubuntu/badge.svg?branch=master)
![Build Status](https://github.com/sakkayaphab/genie/workflows/MacOS/badge.svg?branch=master)
[![GoDoc](https://godoc.org/github.com/sakkayaphab/genie?status.svg)](https://godoc.org/github.com/sakkayaphab/genie)
[![GitHub release (latest by date)](https://img.shields.io/github/v/release/sakkayaphab/genie)](https://github.com/sakkayaphab/genie/releases)
[![Conda](https://img.shields.io/conda/v/bioconda/genie?color=blue&label=Anaconda%20Cloud)](https://anaconda.org/bioconda/genie)


Genie is a collection of tools for a wide-range of genomics analysis tasks. Genie provides a fast, lightweight, and flexible toolkit for reading, writing, and manipulating data.


## Installation
- Download binary from [release page](https://github.com/sakkayaphab/genie/releases)
```sh
chmod +x genie_(platform)_amd64
./genie_(platform)_amd64 -h
```

- Install with [Conda](https://anaconda.org/bioconda/genie)
```sh
conda install -c bioconda genie
genie -h
```


- Build Genie from source
```sh
git clone https://github.com/sakkayaphab/genie.git
cd genie
go build
./genie -h
```

## Usage
```sh
Usage:
  genie [flags]
  genie [command]

Flags:
  -h, --help   help for genie
```



for [user guide][UserGuide]

[UserGuide]:docs/README.md


## License
Genie is distributed under the [GPLv3 License][License]. However, Genie includes several third-party open-source libraries, please see [Third Party Software Notices][LICENSETHIRDPARTY] for details.


[License]:LICENSE
[LICENSETHIRDPARTY]:THIRD-PARTY-LICENSE