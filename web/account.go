package web

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

func PostSignIn(c *gin.Context) {
	username := c.Query("username")
	password := c.Query("password")
	browser := c.Query("browser")
	os := c.Query("os")

	profile,err := acfg.GetProfileByUsername(username)
	if err!=nil {
		c.JSON(http.StatusNotFound, gin.H{
			"message": "not found",
		})
		return
	}

	if profile.Password!=password {
		c.JSON(http.StatusNotFound, gin.H{
			"message": "password not correct",
		})
		return
	}

	bToken,err := acfg.AddTokenByUserIDBrowserOs(profile.UserID,browser,os)
	if err!=nil {
		c.JSON(http.StatusNotFound, gin.H{
			"message": "not found",
		})
		return
	}

	setEasyCookie(c, "userid", strconv.FormatUint(bToken.UserID, 10),31104000, true)
	setEasyCookie(c, "tokenid", strconv.FormatUint(bToken.TokenID, 10),31104000, true)
	setEasyCookie(c, "primarytoken", bToken.PrimaryToken, 31104000, true)
	setEasyCookie(c, "secondarytoken", bToken.SecondaryToken, 31104000, false)

	c.JSON(200, gin.H{
		"message": "success",
	})
}

func GetProfile(c *gin.Context) {
	bToken,err := ManageTokenFromContext(c)
	if err!=nil {
		c.JSON(http.StatusNotFound, gin.H{
			"message": "not found",
		})
		return
	}

	profile,err := acfg.GetProfileByUserID(bToken.UserID)
	if err!=nil {
		c.JSON(http.StatusNotFound, gin.H{
			"message": "not found",
		})
		return
	}

	c.JSON(200, profile)
}

func PostSignOut(c *gin.Context) {
	bToken,err := ManageTokenFromContext(c)
	if err!=nil {
		c.JSON(http.StatusNotFound, gin.H{
			"message": "not found",
		})
		return
	}


	token,err := acfg.DeleteTokenByTokenID(bToken.TokenID)
	if err!=nil {
		c.JSON(http.StatusNotFound, gin.H{
			"message": "not found",
		})
		return
	}
	removeCookiesSessionClient(c)
	c.JSON(200, token)
}

func PostDeleteToken(c *gin.Context) {
	bToken,err := ManageTokenFromContext(c)
	if err!=nil {
		c.JSON(http.StatusNotFound, gin.H{
			"message": "not found",
		})
		return
	}

	tokenidcookie := c.Query("tokenid")
	if tokenidcookie!="" {
		c.JSON(http.StatusNotFound, gin.H{
			"message": "not found",
		})
		return
	}

	tokenid64, err := strconv.ParseInt(tokenidcookie, 10, 64)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{
			"message": "not found",
		})
		return
	}
	tokenid := uint64(tokenid64)

	tokendb,err := acfg.GetTokenByTokenID(tokenid)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{
			"message": "not found",
		})
		return
	}
	if tokendb.UserID!=bToken.UserID {
		c.JSON(http.StatusNotFound, gin.H{
			"message": "not found",
		})
		return
	}

	token,err := acfg.DeleteTokenByTokenID(tokenid)
	if err!=nil {
		c.JSON(http.StatusNotFound, gin.H{
			"message": "not found",
		})
		return
	}

	c.JSON(200, token)
}

func PostChangePassword(c *gin.Context) {
	bToken,err := ManageTokenFromContext(c)
	if err!=nil {
		c.JSON(http.StatusNotFound, gin.H{
			"message": "not found",
		})
		return
	}

	profile,err := acfg.GetProfileByUserID(bToken.UserID)
	if err!=nil {
		c.JSON(http.StatusNotFound, gin.H{
			"message": "not found",
		})
		return
	}

	password := c.Query("password")
	if password!="" {
		c.JSON(http.StatusNotFound, gin.H{
			"message": "not found",
		})
		return
	}


	acfg.ChangePassword(profile,password)
	token,err := acfg.DeleteTokenByTokenID(bToken.TokenID)
	if err!=nil {
		c.JSON(http.StatusNotFound, gin.H{
			"message": "not found",
		})
		return
	}
	removeCookiesSessionClient(c)
	c.JSON(200, token)
}

func PostVerifyCookie(c *gin.Context) {
	_,err := ManageTokenFromContext(c)
	if err!=nil {
		c.JSON(http.StatusNotFound, gin.H{
			"message": "not found",
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"message": "success",
	})
}