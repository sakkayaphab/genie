package web

import (
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/sakkayaphab/genie/account"
	"strconv"
)

func ManageTokenFromContext(c *gin.Context) (*account.Token,error) {
	tokenbrowser,err := getTokenInfoFromContext(c)
	if err != nil {
		return &account.Token{}, err
	}

	if !checkXCSFToken(c, tokenbrowser.SecondaryToken) {
		return &account.Token{}, errors.New("csf token error")
	}

	tokendb,err := acfg.GetTokenByTokenID(tokenbrowser.TokenID)
	if err != nil {
		removeCookiesSessionClient(c)
		return &account.Token{}, err
	}

	if tokendb.UserID != tokenbrowser.UserID {
		removeCookiesSessionClient(c)
		return &account.Token{}, errors.New("token not correct")
	}

	if tokendb.PrimaryToken != tokenbrowser.PrimaryToken {
		removeCookiesSessionClient(c)
		return &account.Token{}, errors.New("token not correct")
	}

	if tokendb.SecondaryToken != tokenbrowser.SecondaryToken {
		removeCookiesSessionClient(c)
		return &account.Token{}, errors.New("token not correct")
	}

	return tokendb,nil
}

func checkXCSFToken(c *gin.Context, sessionsecondarykey string) bool {
	xsrftoken := c.GetHeader("X-CSRF-Token")

	if xsrftoken != sessionsecondarykey {
		return false
	}

	return true
}

func getTokenInfoFromContext(c *gin.Context) (*account.Token,error) {
	var token account.Token

	tokenidcookie, err := c.Cookie("tokenid")
	if err != nil {
		removeCookiesSessionClient(c)
		return &token, err
	}

	tokenid, err := strconv.ParseUint(tokenidcookie, 10, 64)
	if err != nil {
		removeCookiesSessionClient(c)
		return &token, err
	}
	token.TokenID = tokenid

	useridcookie, err := c.Cookie("userid")
	if err != nil {
		removeCookiesSessionClient(c)
		return &token, err
	}

	userid, err := strconv.ParseUint(useridcookie, 10, 64)
	if err != nil {
		removeCookiesSessionClient(c)
		return &token, err
	}
	token.UserID = userid

	primarytoken, err := c.Cookie("primarytoken")
	if err != nil {
		removeCookiesSessionClient(c)
		return &token, err
	}
	token.PrimaryToken = primarytoken

	secondarytoken, err := c.Cookie("secondarytoken")
	if err != nil {
		removeCookiesSessionClient(c)
		return &token, err
	}
	token.SecondaryToken = secondarytoken


	return &token, err
}

func removeCookiesSessionClient(c *gin.Context) {
	setEasyCookie(c, "userid", "", 0, true)
	setEasyCookie(c, "tokenid", "", 0, true)
	setEasyCookie(c, "primarytoken", "", 0, true)
	setEasyCookie(c, "secondarytoken", "", 0, false)
}
