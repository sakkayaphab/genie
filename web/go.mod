module web

go 1.13

require (
	github.com/gin-contrib/cors v1.3.0 // indirect
	github.com/gin-contrib/static v0.0.0-20191128031702-f81c604d8ac2 // indirect
	github.com/go-playground/universal-translator v0.17.0 // indirect
	github.com/json-iterator/go v1.1.8 // indirect
	github.com/leodido/go-urn v1.2.0 // indirect
	github.com/mattn/go-isatty v0.0.11 // indirect
	github.com/sakkayaphab/genie/account v0.0.0
	github.com/sakkayaphab/genie/filemanager v0.0.0
	golang.org/x/sys v0.0.0-20191218084908-4a24b4065292 // indirect
	gopkg.in/go-playground/validator.v9 v9.30.2 // indirect
	gopkg.in/yaml.v2 v2.2.7 // indirect
)

replace (
	github.com/sakkayaphab/genie/account v0.0.0 => ../account
	github.com/sakkayaphab/genie/filemanager v0.0.0 => ../filemanager
)
