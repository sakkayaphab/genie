package web

import (
	"github.com/sakkayaphab/genie/filemanager"
	"github.com/gin-gonic/gin"
	"net/http"
)

type BindUriGetListFiles struct {
	Path string `uri:"path" binding:"required"`
}

func GetListFilesByFullPath(c *gin.Context)  {
	_,err := ManageTokenFromContext(c)
	if err!=nil {
		c.JSON(http.StatusNotFound, gin.H{
			"message": "not found",
		})
		return
	}

	var binduri BindUriGetListFiles
	if err := c.ShouldBindUri(&binduri); err != nil {
		c.JSON(400, gin.H{"msg": err})
		return
	}
	filepath,err := filemanager.DecodeDoobleQueryEscape(binduri.Path)
	if  err != nil {
		c.JSON(400, gin.H{"msg": err})
		return
	}

	listfiles,err := filemanager.GetListFiles(filepath)
	if err != nil {
		c.JSON(400, gin.H{"msg": err})
		return
	}

	c.JSON(http.StatusOK, listfiles)
	return
}

func GetHomePath(c *gin.Context)  {
	_,err := ManageTokenFromContext(c)
	if err!=nil {
		c.JSON(http.StatusNotFound, gin.H{
			"message": "not found",
		})
		return
	}
	homepath := filemanager.GetHomePath()
	c.JSON(http.StatusOK, gin.H{"homepath": homepath})
	return
}