package web

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-contrib/static"
	"github.com/gin-gonic/gin"
	"github.com/sakkayaphab/genie/account"
	"log"
	"time"
)

var acfg *account.Config
var cfg *Config

func Start() {
	var err error
	cfg,err = ReadConfig()
	if err != nil {
		log.Fatal( err )
	}

	acfg,err = cfg.GenarateAccountConfig()
	if err != nil {
		log.Fatal( err )
	}
	

	r := gin.Default()
	r.Use(cors.New(cors.Config{
		AllowOrigins:     []string{"http://localhost:8080"},
		AllowMethods:     []string{"*"},
		AllowHeaders:     []string{"Origin", "X-CSRF-Token"},
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
		MaxAge:           12 * time.Hour,
	}))
	r.POST("/api/account/signin",PostSignIn)
	r.POST("/api/account/signout",PostSignOut)
	r.GET("/api/account/profile",GetProfile)
	r.POST("/api/account/changepassword",PostChangePassword)
	r.POST("/api/account/deletetoken",PostDeleteToken)
	r.POST("/api/account/verifycookie",PostVerifyCookie)
	r.GET("/api/filemanager/gethomepath",GetHomePath)
	r.GET("/api/filemanager/getlistfiles/fullpath/:path",GetListFilesByFullPath)

	r.GET("/api/ping",ping)

	appDir := "/Users/sakkayaphab/.genie/frontend"
	r.Use(static.Serve("/", static.LocalFile(appDir, false)))
	r.Run(":"+cfg.Network.Port)
}

func setEasyCookie(c *gin.Context, name string, value string, durationSecond int, httponly bool) {
	//fmt.Println(name,value)
	//if os.Getenv("APP_ENV") == "production" {
	//	fmt.Println("APP_ENV", os.Getenv("APP_ENV"))
	//	c.SetCookie(name, value, durationSecond, "/", "123.13.1.3", true, httponly)
	//} else {
		c.SetCookie(name, value, durationSecond, "/", "localhost", false, httponly)
	//}
}

func ping(c *gin.Context) {
	c.JSON(200, gin.H{
		"message": "pong",
	})
}

