package web

import (
	"github.com/sakkayaphab/genie/account"
	"encoding/json"
	"fmt"
	"github.com/sakkayaphab/genie/filemanager"
	"io/ioutil"
	"log"
	"os"
)


type DBConfig struct {
	TokenDB  struct {
		Dialect string `json:"dialect"`
		Args string `json:"args"`
	} `json:"tokendb"`
	AccountDB  struct {
		Dialect string `json:"dialect"`
		Args string `json:"args"`
	} `json:"accountdb"`
}

type Network struct {
	AllowOrigins []string `json:"alloworigins"`
	Port string `json:"port"`
}

type Config struct {
	DBConfig DBConfig `json:"dbconfig"`
	Network Network `json:"network"`
	WorkingDir string `json:"-"`
}

func WriteDBConfig()  {
	cfg := Config{}
	cfg.Network.AllowOrigins = append(cfg.Network.AllowOrigins,"http://localhost:8080")
	cfg.Network.Port = "8000"
	cfg.DBConfig.TokenDB.Dialect = "sqlite3"
	cfg.DBConfig.TokenDB.Args = filemanager.GetHomePath() + "/.genie/databases/account.db"
	cfg.DBConfig.AccountDB.Dialect = "sqlite3"
	cfg.DBConfig.AccountDB.Args = filemanager.GetHomePath() + "/.genie/databases/account.db"
	cfg.WorkingDir = filemanager.GetHomePath() + "/.genie"
	cfg.createDirectory()

	file, err := json.MarshalIndent(cfg, "", " ")
	if err!=nil {
		log.Fatal(err)
	}
	err = ioutil.WriteFile(cfg.WorkingDir+"/config.json", file, 0755)
	if err!=nil {
		log.Fatal(err)
	}
	//dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	//if err != nil {
	//	log.Fatal(err)
	//}
	//fmt.Println(dir)
	fmt.Println("genarated files at :",cfg.WorkingDir)
}

func (cfg *Config) createDirectory()  error {
	err := os.MkdirAll(cfg.WorkingDir, 0755)
	if err!=nil {
		return  err
	}
	err = os.MkdirAll(cfg.WorkingDir+"/databases", 0755)
	if err!=nil {
		return  err
	}
	err = os.MkdirAll(cfg.WorkingDir+"/frontend", 0755)
	if err!=nil {
		return  err
	}
	err = os.MkdirAll(cfg.WorkingDir+"/recipes", 0755)
	if err!=nil {
		return  err
	}
	return  nil
}

func AddUser(username, password string)  {
	
}

func ReadConfig()  (*Config,error) {
	jsonFile, err := os.Open(filemanager.GetHomePath() + "/.genie/config.json")
	if err != nil {
		return &Config{},err
	}
	defer jsonFile.Close()
	byteValue, _ := ioutil.ReadAll(jsonFile)
	var config Config
	json.Unmarshal(byteValue, &config)

	return  &config,nil
}

func (cfg *Config) ToPrettyJson()  (string,error){
	file, err := json.MarshalIndent(cfg, "", " ")
	if err!=nil {
		return  "",err
	}
	return  string(file),nil
}

func (cfg *Config) GenarateAccountConfig()  (*account.Config,error){
	acfg = account.NewConfig()
	acfg.TokenDB.Dialect = cfg.DBConfig.TokenDB.Dialect
	acfg.TokenDB.Args = cfg.DBConfig.TokenDB.Args
	acfg.AccountDB.Dialect = cfg.DBConfig.AccountDB.Dialect
	acfg.AccountDB.Args = cfg.DBConfig.AccountDB.Args

	return  acfg,nil
}