package hts

import (
	"errors"
	"github.com/biogo/hts/sam"
)

func GetReferenceByChrName(refs []*sam.Reference,name string)  (*sam.Reference,error){
	for _,x := range refs {
		if x.Name()==name {
			return  x,nil
		}
	}

	return nil,errors.New("Not found the name reference")
}