package hts

import (
	"log"
	"os"
	"testing"
)

func TestConvertSamToBam(t *testing.T) {
	sampath := "testdata/example.sam"
	bampath := "testdata/example_converted.bam"
	os.Remove(bampath)
	err := ConvertSamToBam(sampath,bampath)
	if err!=nil {
		log.Fatal(err)
	}
	os.Remove(bampath)
}
