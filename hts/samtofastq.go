package hts

import (
	"bufio"
	"errors"
	"fmt"
	"github.com/biogo/hts/sam"
	"io"
	"os"
)

func ConvertSamToFastq(filepath, fq1path, fq2path string, reqFlag, excFlag uint16) (uint64,error ){
	f, err := os.Open(filepath)
	if err != nil {
		return 0,err
	}
	defer f.Close()

	b, err := sam.NewReader(f)
	if err != nil {
		return 0,err
	}

	fq1, err := os.Create(fq1path)
	if err != nil {
		return 0,err
	}
	defer fq1.Close()

	var fq2 *os.File
	if fq2path!="" {
		fq2, err = os.Create(fq2path)
		if err != nil {
			return 0,err
		}
	}
	defer fq2.Close()

	var count uint64
	for {
		rec, err := b.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			return count,err
		}

		if rec.Flags&sam.Flags(reqFlag) == sam.Flags(reqFlag) && rec.Flags&sam.Flags(excFlag) == 0 {

			var firstread bool
			var secondread bool
			if rec.Flags&sam.Flags(64) == sam.Flags(64) {
				firstread = true
			}
			if rec.Flags&sam.Flags(128) == sam.Flags(128) {
				secondread = true
			}

			if firstread==true && secondread == true {
				return  count,errors.New("found first read and second read")
			}

			if fq2path == "" {
				fastqcon := ConvertRecordToFastQ(rec, firstread)
				err = fastqcon.writeFile(fq1)
				count++
				if err != nil {
					return count,err
				}
			} else {
				// first read in pair
				if firstread {
					fastqcon := ConvertRecordToFastQ(rec, true)
					err = fastqcon.writeFile(fq1)
					count++
					if err != nil {
						return count,err
					}
				}

				// second read in pair
				if secondread {
					fastqcon := ConvertRecordToFastQ(rec, false)
					err = fastqcon.writeFile(fq2)
					count++
					if err != nil {
						return count,err
					}
				}
			}
		}
	}

	return count,nil
}

func formatQual(q []byte) []byte {
	for _, v := range q {
		if v != 0xff {
			a := make([]byte, len(q))
			for i, p := range q {
				a[i] = p + 33
			}
			return a
		}
	}
	return []byte{'*'}
}

type FastQConvert struct {
	Name      string
	Seq       string
	Separator string
	Qual      string
}

func (fqc *FastQConvert) writeFile(file *os.File) error {
	w := bufio.NewWriter(file)
	fmt.Fprintln(w, fqc.Name)
	fmt.Fprintln(w, fqc.Seq)
	fmt.Fprintln(w, fqc.Separator)
	fmt.Fprintln(w, fqc.Qual)
	return w.Flush()
}

func ConvertRecordToFastQ(rec *sam.Record, first bool) FastQConvert {
	var name string
	if first {
		name = "@" + rec.Name + "#0" + "/1"
	} else {
		name = "@" + rec.Name + "#0" + "/2"
	}
	return FastQConvert{
		// Name illumina < v1.4
		Name:      name,
		Seq:       string(rec.Seq.Expand()),
		Separator: "+",
		Qual:      string(formatQual(rec.Qual)),
	}
}
