package hts

import (
	"log"
	"testing"
)

func TestCountBam(t *testing.T) {
	sampath := "testdata/example.bam"
	count,err := CountBam(sampath,0,0)
	if err!=nil {
		log.Fatal(err)
	}
	if count != 200 {
		t.Error("count sam invalid")
	}
}
