package hts

import (
	"bufio"
	"errors"
	"fmt"
	"github.com/biogo/hts/bam"
	"github.com/biogo/hts/bgzf"
	"github.com/biogo/hts/sam"
	"io"
	"log"
	"os"
	"strconv"
	"strings"
)

type ScopeRead struct {
	Chr string
	OnlyChr bool
	Begin int
	End int
}

func ParseStringToScope(scope string)  (ScopeRead,error){
	if scope=="" {
		return ScopeRead{},errors.New("scope is emtry")
	}

	s := strings.Split(scope, ":")
	if len(s)>2 {
		return ScopeRead{},errors.New("invalid scope")
	}

	if len(s)==1 {
		return ScopeRead{Chr:s[0],OnlyChr:true},nil
	}

	r := strings.Split(s[1], "-")
	pos, err := strconv.Atoi(r[0])
	if err!=nil {
		return ScopeRead{},errors.New("invalid pos")
	}
	end, err := strconv.Atoi(r[1])
	if err!=nil {
		return ScopeRead{},errors.New("invalid end")
	}

	return ScopeRead{Chr:s[0],Begin:pos,End:end},nil
}

func ReadfileBam(filepath string,headeronly bool,includeheader bool,scopes []ScopeRead,limitreads int,humanreadable bool) error{
	f, err := os.Open(filepath)
	if err != nil {
		return err
	}
	defer f.Close()
	ok, err := bgzf.HasEOF(f)
	if err != nil {
		return err
	}
	if !ok {
		return errors.New("the file has no bgzf magic block: may be truncated")
	}
	b, err := bam.NewReader(f, 0)
	if err != nil {
		return  err
	}
	defer b.Close()

	if headeronly {
		BamShowHeader(b)
		return nil
	}

	if includeheader {
		BamShowHeader(b)
	}

	if len(scopes)==0 {
		return FullReadBam(b,limitreads,humanreadable)
	}

	fileIndex, err := os.Open(filepath+".bai")
	if err != nil {
		log.Fatal(err)
	}
	defer fileIndex.Close()
	rIndex := bufio.NewReader(fileIndex)
	bamIndex,err := bam.ReadIndex(rIndex)
	if err != nil {
		return  err
	}

	var countRead int

	for _,x := range scopes {
		if x.OnlyChr {
			err := ReadByChr(b,bamIndex,x.Chr,&countRead,&limitreads,humanreadable)
			if err!=nil {
				return err
			}
		} else {
			err := ReadByRange(b,bamIndex,x.Chr,x.Begin,x.End,&countRead,&limitreads,humanreadable)
			if err!=nil {
				return err
			}
		}
	}

	return nil
}

func BamShowHeader(reader *bam.Reader)  error{
	bi,err := reader.Header().MarshalText()
	if err != nil {
		return err
	}
	fmt.Println(string(bi))

	return  nil
}

func ReadByChr(b *bam.Reader,bamIndex *bam.Index,chr string,countRead *int,limitRead *int, humanreadable bool)  error {
	ref,err := GetReferenceByChrName(b.Header().Refs(),chr)
	if err != nil {
		return err
	}

	chunks,err := bamIndex.Chunks(ref,0,ref.Len())
	if err != nil {
		return err
	}

	bx, err := bam.NewIterator(b, chunks)
	if err != nil {
		return err
	}

	for bx.Next() {
		if (bx.Record().Ref.Name()!=chr) {
			break
		}

		if *limitRead>0 {
			if *countRead>=*limitRead {
				break
			}
		}
		*countRead++

		err = ShowRead(bx.Record(),humanreadable)
		if err !=nil {
			return  err
		}
	}

	return  nil
}

func ReadByRange(b *bam.Reader,bamIndex *bam.Index,chr string,pos, end int,countRead *int,limitRead *int, humanreadable bool)  error{
	ref,err := GetReferenceByChrName(b.Header().Refs(),chr)
	if err != nil {
		return err
	}

	chunks,err := bamIndex.Chunks(ref,pos,end)
	if err != nil {
		return err
	}

	bx, err := bam.NewIterator(b, chunks)
	if err != nil {
		return err
	}

	for bx.Next() {
		if (bx.Record().Ref.Name()!=chr) {
			break
		}
		if (bx.Record().Pos<pos) {
			continue
		}
		if (bx.Record().Pos>end) {
			break
		}
		if *limitRead>0 {
			if *countRead>=*limitRead {
				break
			}
		}
		*countRead++

		err = ShowRead(bx.Record(),humanreadable)
		if err !=nil {
			return  err
		}
	}

	return  nil
}

func FullReadBam(reader *bam.Reader,limitread int, humanreadable bool)  error{
	var count int
	for {
		rec, err := reader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			return err
		}

		if (limitread>0) {
			if count>=limitread {
				break
			}
		}
		count++

		err = ShowRead(rec,humanreadable)
		if err !=nil {
			return  err
		}
	}
	return  nil
}

func ShowRead(rec *sam.Record, humanreadable bool)  error{
	if humanreadable {
		fmt.Println(rec.String())
		return nil
	}

	bytesRecord,err := rec.MarshalText()
	if err!=nil {
		return  err
	}

	fmt.Println(string(bytesRecord))
	return  nil
}
