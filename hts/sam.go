package hts

import (
	"fmt"
	"github.com/biogo/hts/sam"
	"io"
	"os"
)

func ReadfileSam(filepath string,headeronly bool,includeheader bool,scopes []ScopeRead,limitreads int64,humanreadable bool) error{
	f, err := os.Open(filepath)
	if err != nil {
		return err
	}
	defer f.Close()

	b, err := sam.NewReader(f)
	if err != nil {
		return  err
	}

	if headeronly {
		SamShowHeader(b)
		return nil
	}

	if includeheader {
		SamShowHeader(b)
	}

	if len(scopes)==0 {
		return FullReadSam(b,limitreads,humanreadable)
	}

	var countRead int64

	for _,x := range scopes {
		if x.OnlyChr {
			err := ReadSamByChr(b,x.Chr,&countRead,&limitreads,humanreadable)
			if err!=nil {
				return err
			}
		} else {
			err := ReadSamByRange(b,x.Chr,x.Begin,x.End,&countRead,&limitreads,humanreadable)
			if err!=nil {
				return err
			}
		}
	}
	//fmt.Println(count)

	return nil
}


func ReadSamByRange(b *sam.Reader,chr string,pos, end int,countRead *int64,limitRead *int64, humanreadable bool)  error{

	_,err := GetReferenceByChrName(b.Header().Refs(),chr)
	if err != nil {
		return err
	}
	for {
		rec, err := b.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			return err
		}

		if (rec.Ref.Name()!=chr) {
			break
		}
		if (rec.Pos<pos) {
			continue
		}
		if (rec.Pos>end) {
			break
		}
		if *limitRead>0 {
			if *countRead>=*limitRead {
				break
			}
		}
		*countRead++

		err = ShowRead(rec,humanreadable)
		if err !=nil {
			return  err
		}
	}


	return  nil
}

func ReadSamByChr(b *sam.Reader,chr string,countRead *int64,limitRead *int64, humanreadable bool)  error {
	_,err := GetReferenceByChrName(b.Header().Refs(),chr)
	if err != nil {
		return err
	}
	for {
		rec, err := b.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			return err
		}

		if (*limitRead>0) {
			if *countRead>=*limitRead {
				break
			}
		}
		*countRead++

		err = ShowRead(rec,humanreadable)
		if err !=nil {
			return  err
		}
	}

	return  nil
}

func FullReadSam(reader *sam.Reader,limitread int64, humanreadable bool)  error{
	var count int64
	for {
		rec, err := reader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			return err
		}

		if (limitread>0) {
			if count>=limitread {
				break
			}
		}
		count++

		err = ShowRead(rec,humanreadable)
		if err !=nil {
			return  err
		}
	}
	return  nil
}

func SamShowHeader(reader *sam.Reader)  error{
	bi,err := reader.Header().MarshalText()
	if err != nil {
		return err
	}
	fmt.Println(string(bi))

	return  nil
}