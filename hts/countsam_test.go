package hts

import (
	"log"
	"testing"
)

func TestCountSam(t *testing.T) {
	sampath := "testdata/example.sam"
	count,err := CountSam(sampath,0,0)
	if err!=nil {
		log.Fatal(err)
	}
	if count != 200 {
		t.Error("count sam invalid")
	}
}
