package hts

import (
	"github.com/biogo/hts/sam"
	"io"
	"os"
)

func CountSam(filepath string,reqFlag, excFlag uint16)  (uint64,error){
	f, err := os.Open(filepath)
	if err != nil {
		return 0,err
	}
	defer f.Close()

	b, err := sam.NewReader(f)
	if err != nil {
		return 0,err
	}
	var count uint64
	for {
		rec, err := b.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			return count,err
		}
		if rec.Flags&sam.Flags(reqFlag) == sam.Flags(reqFlag) && rec.Flags&sam.Flags(excFlag) == 0 {
			count++
		}
	}

	return count,nil
}
