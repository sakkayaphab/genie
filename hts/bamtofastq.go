package hts

import (
	"errors"
	"github.com/biogo/hts/bam"
	"github.com/biogo/hts/bgzf"
	"github.com/biogo/hts/sam"
	"io"
	"os"
)

func ConvertBamToFastq(filepath, fq1path, fq2path string, reqFlag, excFlag uint16) (uint64,error) {
	f, err := os.Open(filepath)
	if err != nil {
		return 0,err
	}
	defer f.Close()
	ok, err := bgzf.HasEOF(f)
	if err != nil {
		return 0,err
	}
	if !ok {
		return 0,errors.New("the file has no bgzf magic block: may be truncated")
	}
	b, err := bam.NewReader(f, 0)
	if err != nil {
		return  0,err
	}
	defer b.Close()

	fq1, err := os.Create(fq1path)
	if err != nil {
		return 0,err
	}
	defer fq1.Close()

	var fq2 *os.File
	if fq2path!="" {
		fq2, err = os.Create(fq2path)
		if err != nil {
			return 0,err
		}
	}
	defer fq2.Close()

	var count uint64
	for {
		rec, err := b.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			return count,err
		}

		if rec.Flags&sam.Flags(reqFlag) == sam.Flags(reqFlag) && rec.Flags&sam.Flags(excFlag) == 0 {

			var firstread bool
			var secondread bool
			if rec.Flags&sam.Flags(64) == sam.Flags(64) {
				firstread = true
			}
			if rec.Flags&sam.Flags(128) == sam.Flags(128) {
				secondread = true
			}

			if firstread==true && secondread == true {
				return  count,errors.New("found first read and second read")
			}

			if fq2path == "" {
				fastqcon := ConvertRecordToFastQ(rec, firstread)
				err = fastqcon.writeFile(fq1)
				count++
				if err != nil {
					return count,err
				}
			} else {
				// first read in pair
				if firstread {
					fastqcon := ConvertRecordToFastQ(rec, true)
					err = fastqcon.writeFile(fq1)
					count++
					if err != nil {
						return count,err
					}
				}

				// second read in pair
				if secondread {
					fastqcon := ConvertRecordToFastQ(rec, false)
					err = fastqcon.writeFile(fq2)
					count++
					if err != nil {
						return count,err
					}
				}
			}
		}
	}

	return count,nil
}
