package hts

import (
	"bufio"
	"fmt"
	"github.com/biogo/hts/bam"
	"io"
	"os"
)

func ConvertBamToSam(filepath, sampath string, reqFlag, excFlag uint16) (uint64, error) {
	f, err := os.Open(filepath)
	if err != nil {
		return 0, err
	}
	defer f.Close()

	b, err := bam.NewReader(f,0)
	if err != nil {
		return 0, err
	}

	samwriter, err := os.Create(sampath)
	if err != nil {
		return 0, err
	}
	defer samwriter.Close()

	bi,err := b.Header().MarshalText()
	if err != nil {
		return 0,err
	}

	datawriter := bufio.NewWriter(samwriter)
	datawriter.Write(bi)

	//const defaultBufSize = 4096
	//buf := make([]byte, defaultBufSize)
	//buf[len(buf)-1] = '\n'
	//w := bufio.NewWriterSize(samreader, len(buf))

	w := bufio.NewWriter(samwriter)
	//fmt.Fprintln(w, string(bi))
	_, err = w.Write(bi)
	if err != nil {
		return 0,err
	}
	//err = w.Flush()
	//if err != nil {
	//	return 0,err
	//}
	fmt.Print("MarshalText")
	var count uint64
	//var buffer [][]byte
	s := "\n"
	for {
		rec, err := b.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			return count, err
		}

		//if rec.Flags&sam.Flags(reqFlag) == sam.Flags(reqFlag) && rec.Flags&sam.Flags(excFlag) == 0 {
		//
		//	var firstread bool
		//	var secondread bool
		//	if rec.Flags&sam.Flags(64) == sam.Flags(64) {
		//		firstread = true
		//	}
		//	if rec.Flags&sam.Flags(128) == sam.Flags(128) {
		//		secondread = true
		//	}
		//
		//	if firstread == true && secondread == true {
		//		return count, errors.New("found first read and second read")
		//	}

			bytesRecord,err := rec.MarshalText()
			if err!=nil {
				return  count,err
			}

			recBytesNewline := append(bytesRecord,[]byte(s)...)
			_, err = w.Write(recBytesNewline)
			if err != nil {
				return 0,err
			}
			//_, err = datawriter.Write(bytesRecord)
			//if err != nil {
			//	return 0,err
			//}
			//w := bufio.NewWriter(samreader)
			//fmt.Fprintln(w, string(bytesRecord))
			//buffer = append(buffer,bytesRecord)
			//if len(buffer)>1000 {
			//	for _,x := range buffer {
			//		fmt.Fprintln(w, string(x))
			//		//w.Write(x)
			//		//w.Write()
			//		//w.Write(string(x))
			//	}
			//	buffer = nil
			//}

			//if count%10000==0 {
			//	datawriter.Flush()
			//}

			//if err != nil {
			//	return 0,err
			//}

			count++
			if err != nil {
				return count, err
			}
		//}
	}
	err = datawriter.Flush()
	if err != nil {
		return 0,err
	}
	return count, nil

}