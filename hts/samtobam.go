package hts

import (
	"github.com/biogo/hts/bam"
	"github.com/biogo/hts/sam"
	"io"
	"os"
)

func ConvertSamToBam(sampath string,bampath string)  error{
	f, err := os.Open(sampath)
	if err != nil {
		return err
	}
	defer f.Close()

	b, err := sam.NewReader(f)
	if err != nil {
		return err
	}

	fBamPath, err := os.Create(bampath)
	if err != nil {
		return err
	}
	defer fBamPath.Close()

	writer,err := bam.NewWriter(fBamPath,b.Header(),0)
	if err != nil {
		return err
	}
	defer writer.Close()

	for {
		rec, err := b.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			return  err
		}

		writer.Write(rec)
	}

	return  nil
}