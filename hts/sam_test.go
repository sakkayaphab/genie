package hts

import (
	"log"
	"testing"
)

func TestOpenSAM(t *testing.T) {
	err := ReadfileSam("testdata/example.sam",true,false,nil,0,true)
	if err!=nil {
		log.Fatal(err)
	}
}