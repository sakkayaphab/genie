package hts

import (
	"log"
	"os"
	"testing"
)

func TestConvertSamToFastq(t *testing.T) {
	sampath := "testdata/example.sam"
	fq1path := "testdata/example_sam_1.fq"
	fq2path := "testdata/example_sam_2.fq"
	os.Remove(fq1path)
	os.Remove(fq2path)
	count,err := ConvertSamToFastq(sampath,fq1path,fq2path,0,0)
	if err!=nil {
		log.Fatal(err)
	}
	if count != 200 {
		t.Error("bam convert to fastq invalid count")
	}
	os.Remove(fq1path)
	os.Remove(fq2path)
}

func TestConvertSamToFastqSingle(t *testing.T) {
	sampath := "testdata/example.sam"
	fq1path := "testdata/example_sam_single.fq"
	fq2path := ""
	os.Remove(fq1path)
	count,err := ConvertSamToFastq(sampath,fq1path,fq2path,0,0)
	if err!=nil {
		log.Fatal(err)
	}
	if count != 200 {
		t.Error("bam convert to fastq invalid count")
	}
	os.Remove(fq1path)
}