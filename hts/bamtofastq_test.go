package hts

import (
	"log"
	"os"
	"testing"
)

func TestConvertBamToFastq(t *testing.T) {
	sampath := "testdata/example.bam"
	fq1path := "testdata/example_bam_1.fq"
	fq2path := "testdata/example_bam_2.fq"
	os.Remove(fq1path)
	os.Remove(fq2path)
	count,err := ConvertBamToFastq(sampath,fq1path,fq2path,0,0)
	if err!=nil {
		log.Fatal(err)
	}
	if count != 200 {
		t.Error("bam convert to fastq invalid count")
	}
	os.Remove(fq1path)
	os.Remove(fq2path)
}

func TestConvertBamToFastqSingle(t *testing.T) {
	sampath := "testdata/example.bam"
	fq1path := "testdata/example_bam_single.fq"
	fq2path := ""
	os.Remove(fq1path)
	count,err := ConvertBamToFastq(sampath,fq1path,fq2path,0,0)
	if err!=nil {
		log.Fatal(err)
	}
	if count != 200 {
		t.Error("bam convert to fastq invalid count")
	}
	os.Remove(fq1path)
}