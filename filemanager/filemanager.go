package filemanager

import (
	"io/ioutil"
	"log"
	"net/url"
	"os/user"
	"path/filepath"
	"time"
)

type FileInfo struct {
	Filename string `json:"filename"`
	IsDir bool `json:"isdir"`
	Size int64 `json:"size"`
	Mode string `json:"mode"`
	ModTime time.Time `json:"modtime"`
	FullPath string `json:"fullpath"`
}

func EncodeDoobleQueryEscape(data string)  string{
	qes := url.QueryEscape(data)
	return url.QueryEscape(qes)
}

func DecodeDoobleQueryEscape(data string)  (string,error){
	qes,err := url.QueryUnescape(data)
	if err != nil {
		return "",err
	}

	return url.QueryUnescape(qes)
}

func GetListFiles(path string) ([]FileInfo,error){
	var fileinfos []FileInfo
	path = filepath.Clean(path)
	files, err := ioutil.ReadDir(path)
	if err != nil {
		return fileinfos,err
	}

	for _, f := range files {
		tempFileInfo := FileInfo{
			Filename:f.Name(),
			IsDir:f.IsDir(),
			Size:f.Size(),
			ModTime:f.ModTime(),
			Mode:f.Mode().String(),
			FullPath:path+"/"+f.Name(),
		}

		fileinfos = append(fileinfos,tempFileInfo)
	}

	return fileinfos,nil
}

func GetHomePath()  string{
	usr, err := user.Current()
	if err != nil {
		log.Fatal( err )
	}
	return usr.HomeDir
}