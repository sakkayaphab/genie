package filemanager

import (
	"io"
	"net/http"
	"os"
)

func DownloadByHttp(url, outputpath string)  error{
	out, err := os.Create(outputpath)
	if err!=nil {
		return err
	}
	defer out.Close()

	resp, err := http.Get(url)
	if err!=nil {
		return err
	}
	defer resp.Body.Close()

	_, err = io.Copy(out, resp.Body)
	if err!=nil {
		return err
	}

	return nil
}