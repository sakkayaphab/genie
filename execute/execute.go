package execute

import (
	"fmt"
	"os/exec"
)


type ExecuteCommand struct {
	Name string
	Path string
	Arguments []string
}

func CheckAll()  {
	var eclist []ExecuteCommand
	var samtoolsEx ExecuteCommand
	samtoolsEx.Name = "samtools"
	samtoolsEx.Path = "/data/users/duangdao/EXTERNAL_SOFTWARE/samtools-1.9/samtools"
	samtoolsEx.Arguments = append(samtoolsEx.Arguments,"help")
	eclist = append(eclist,samtoolsEx)
	var gppEx ExecuteCommand
	gppEx.Name = "g++"
	gppEx.Path = "/data/users/duangdao/anaconda3/envs/kan/bin/g++"
	gppEx.Arguments = append(gppEx.Arguments,"--version")
	eclist = append(eclist,gppEx)

	for _,x := range eclist{
		out,err := x.RunExec()
		if err!=nil {
			fmt.Println(string(out))
			fmt.Println(err)
		}
	}

	//usr, err := user.Current()
	//if err != nil {
	//	log.Fatal( err )
	//}
	//fmt.Println(usr.HomeDir)
}

func (e *ExecuteCommand) RunExec()  ([]byte,error) {
	out, err := exec.Command(e.Path,e.Arguments...).CombinedOutput()
	return  out,err
}