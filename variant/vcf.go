package variant

import (
	"fmt"
	"log"
	"os"
	"strconv"

	"github.com/olekukonko/tablewriter"
	"github.com/sakkayaphab/vcfgo"
)

type VcfManagement struct {
	OnlySv      bool
	MinSvLength int
	MaxSvLength int
}

func (vm *VcfManagement) ManageVCF(filepath string, outputpath string) {
	rconfig := vcfgo.InitRecordConfig()
	rconfig.OnlySv = true
	rconfig.MinSvLength = 50
	vcf, err := vcfgo.ReadFileWithConfig(filepath, rconfig)
	if err != nil {
		log.Fatal(err)
	}

	if outputpath != "" {
		vcf.WriteVcfFile(outputpath)
	} else {
		for _, x := range vcf.MetaLine {
			fmt.Println(x)
		}

		for _, x := range vcf.Record {
			fmt.Println(x.GetSource())
		}
	}
}

func ViewStatsVCF(filepath string) {
	rconfig := vcfgo.InitRecordConfig()
	vcf, err := vcfgo.ReadFileWithConfig(filepath, rconfig)
	if err != nil {
		log.Fatal(err)
	}

	vcfstatslist, err := vcf.CalculateStats()
	if err != nil {
		log.Fatal(err)
	}

	var data [][]string
	for _, x := range vcfstatslist {
		var templistText []string

		if x.SvType != "" {
			templistText = append(templistText, x.SvType)
		} else {
			templistText = append(templistText, "?")
		}
		templistText = append(templistText, strconv.Itoa(len(x.VCF.Record)))
		if x.SvType == "BND" {
			templistText = append(templistText, "")
			templistText = append(templistText, "")
		} else if x.SvType == "" {
			templistText = append(templistText, "")
			templistText = append(templistText, "")
		} else {
			templistText = append(templistText, String(x.MinSvLength))
			templistText = append(templistText, String(x.MaxSvLength))
		}

		data = append(data, templistText)
	}

	table := tablewriter.NewWriter(os.Stdout)
	table.SetHeader([]string{"SVTYPE", "Number", "Min SVLENGTH", "Max SVLENGTH"})
	table.SetBorder(false)
	table.AppendBulk(data)
	table.Render()
}
