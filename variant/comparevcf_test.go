package variant

import "testing"

func TestRECIPROCALVALIDATION(t *testing.T) {
	tables := []struct {
		Pos        int
		End        int
		overlapped int
		TargetPos  int
		TargetEnd  int
		Expected   bool
	}{
		{10000, 20000, 70, 10000, 20000, true},

		{12000, 21000, 70, 10000, 20000, true},
		{15000, 25000, 70, 10000, 20000, false},

		{10000, 20000, 70, 12000, 21000, true},
		{5000, 14000, 70, 12000, 21000, false},

		{10000, 20000, 70, 12000, 19000, true},
		{10000, 25000, 70, 12000, 19000, false},

		{12000, 19000, 70, 10000, 20000, true},
		{14000, 15000, 70, 10000, 20000, false},
		{20001, 20002, 70, 10000, 20000, false},
		{9998, 9999, 70, 10000, 20000, false},
	}

	for _, table := range tables {

		total := reciprocalValidation(table.Pos, table.End, table.overlapped, table.TargetPos, table.TargetEnd)
		if total != table.Expected {
			t.Errorf("reciprocal validation was incorrect, pos: %d, end: %d, overlapped:%d, TargetPos:%d, TargetEnd:%d", table.Pos, table.End, table.overlapped, table.TargetPos, table.TargetEnd)
		}
	}
}

func TestBetween(t *testing.T) {
	tables := []struct {
		Pos        int
		overlapped int
		TargetPos  int
		Expected   bool
	}{
		{100, 50, 150, true},
		{100, 50, 50, true},
		{100, 50, 100, true},
		{100, 50, 49, false},
		{100, 50, 151, false},
	}
	for _, table := range tables {
		total := checkBetween(table.Pos, table.TargetPos, table.overlapped, table.overlapped)
		if total != table.Expected {
			t.Errorf("reciprocal validation was incorrect, pos: %d overlapped:%d, TargetPos:%d", table.Pos, table.overlapped, table.TargetPos)
		}
	}
}
