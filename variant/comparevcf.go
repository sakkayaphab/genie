package variant

import (
	"fmt"
	"github.com/sakkayaphab/vcfgo"
	"log"
)

func CompareBedAndVCF(filepaths []string, onlySvtype string, reciprocal int, overlap int) {

	firstVCF := readBEDFile(filepaths[0])
	firstBench := convertBEDToVCFBenchmark(firstVCF, onlySvtype)
	secondVCF := readVCFFile(filepaths[1])
	secondBench := convertVCFToVCFBenchmark(secondVCF, onlySvtype)

	MasterGroups := SplitBySvtype(&firstBench)
	removeDuplicatePosRange(&MasterGroups, 0)

	groups := SplitBySvtype(&secondBench)
	removeDuplicatePosRange(&groups, 0)

	for _, y := range groups {
		for _, z := range MasterGroups {
			if y.Svtype == z.Svtype {
				tmpMaster := z
				tmpMaster.VCFBenchmark = make([]VCFBenchmark, len(z.VCFBenchmark))
				copy(tmpMaster.VCFBenchmark, z.VCFBenchmark)
				limitSVLength(&tmpMaster, 50)
				limitSVLength(&y, 50)
				// fmt.Println("VCFBenchmark :", len(y.VCFBenchmark))

				match, _, _, secondaryUnmatch := compareVariant(tmpMaster, y, reciprocal, overlap)
				numMatch := len(match)
				// numUnmatch := len(unmatch)
				// numSecondMatch := len(secondaryMatch)
				numSecondUnmatch := len(secondaryUnmatch)

				// fmt.Println("=============================================================")
				fmt.Println("A file :", filepaths[0], "Variants :", len(tmpMaster.VCFBenchmark))
				fmt.Println("B file :", filepaths[1], "Variants :", len(y.VCFBenchmark))
				fmt.Println("SVTYPE :", y.Svtype)
				fmt.Println("True Positives :", numMatch)
				fmt.Println("False Positives :", numSecondUnmatch)
				fmt.Println("-----------------------------------------------------------------")

			}
		}
	}
}

func CompareVcfAndVcf(filepaths []string, onlySvtype string, reciprocal, overlap int, outputpl, outputpu, outputsl, outputsu string) {

	firstVCF := readVCFFile(filepaths[0])
	firstBench := convertVCFToVCFBenchmark(firstVCF, onlySvtype)
	secondVCF := readVCFFile(filepaths[1])
	secondBench := convertVCFToVCFBenchmark(secondVCF, onlySvtype)

	MasterGroups := SplitBySvtype(&firstBench)
	removeDuplicatePosRange(&MasterGroups, 0)

	groups := SplitBySvtype(&secondBench)
	removeDuplicatePosRange(&groups, 0)

	var tempPrimaryOverlapped, tempPrimaryUnoverlapped, tempSecondaryOverlapped, tempSecondaryUnoverlapped []VCFBenchmark
	for _, y := range groups {
		for _, z := range MasterGroups {
			if y.Svtype == z.Svtype {
				tmpMaster := z
				tmpMaster.VCFBenchmark = make([]VCFBenchmark, len(z.VCFBenchmark))
				copy(tmpMaster.VCFBenchmark, z.VCFBenchmark)
				limitSVLength(&tmpMaster, 50)
				limitSVLength(&y, 50)
				// fmt.Println("VCFBenchmark :", len(y.VCFBenchmark))

				match, _, secondaryMatch, secondaryUnmatch := compareVariant(tmpMaster, y, reciprocal, overlap)
				numMatch := len(match)
				// numUnmatch := len(unmatch)
				// numSecondMatch := len(secondaryMatch)
				numSecondUnmatch := len(secondaryUnmatch)

				// fmt.Println("=============================================================")
				fmt.Println("A file :", filepaths[0], "Variants :", len(tmpMaster.VCFBenchmark))
				fmt.Println("B file :", filepaths[1], "Variants :", len(y.VCFBenchmark))
				fmt.Println("SVTYPE :", y.Svtype)
				fmt.Println("True Positives :", numMatch)
				fmt.Println("False Positives :", numSecondUnmatch)
				fmt.Println("-----------------------------------------------------------------")

				tempPrimaryOverlapped = append(tempPrimaryOverlapped, *GetOnlyVerify(&tmpMaster.VCFBenchmark, true)...)
				tempPrimaryUnoverlapped = append(tempPrimaryUnoverlapped, *GetOnlyVerify(&tmpMaster.VCFBenchmark, false)...)
				tempSecondaryOverlapped = append(tempSecondaryOverlapped, secondaryMatch...)
				tempSecondaryUnoverlapped = append(tempSecondaryUnoverlapped, secondaryUnmatch...)

			}
		}
	}

	if outputpl != "" {
		WriteFileVCFBenchmark(outputpl, &firstVCF.MetaLine, &tempPrimaryOverlapped)
	}
	if outputpu != "" {
		WriteFileVCFBenchmark(outputpu, &firstVCF.MetaLine, &tempPrimaryUnoverlapped)
	}
	if outputsl != "" {
		WriteFileVCFBenchmark(outputsl, &secondVCF.MetaLine, &tempSecondaryOverlapped)
	}
	if outputsu != "" {
		WriteFileVCFBenchmark(outputsu, &secondVCF.MetaLine, &tempSecondaryUnoverlapped)
	}
}

func GetOnlyVerify(vcfbenlist *[]VCFBenchmark, only bool) *[]VCFBenchmark {
	var temp []VCFBenchmark
	for _, x := range *vcfbenlist {
		if x.Verify == only {
			temp = append(temp, x)
		}
	}

	return &temp
}

func WriteFileVCFBenchmark(outputpath string, metaline *[]string, vcfbench *[]VCFBenchmark) {

	var record []*string
	for _, x := range *vcfbench {
		record = append(record, x.Source)
	}
	vcfpackage, _ := vcfgo.ConvertMetaLineAndRecordToVCF(metaline, &record)
	vcfpackage.WriteVcfFile(outputpath)
}

func limitSVLength(benchmark *VCFBenchmarkGroup, sizelimit int) {
	var temp VCFBenchmarkGroup
	if benchmark.Svtype == "DEL" {

	} else if benchmark.Svtype == "DUP" {

	} else if benchmark.Svtype == "INV" {

	} else {
		return
	}

	for _, x := range benchmark.VCFBenchmark {

		if x.End-x.Pos < 0 {
			// fmt.Println(x)
			// log.Fatalln("End-Pos < 0")
			continue
		}

		if x.End-x.Pos >= sizelimit {
			temp.VCFBenchmark = append(temp.VCFBenchmark, x)
		}
	}

	benchmark.VCFBenchmark = temp.VCFBenchmark
}

func compareVariant(master VCFBenchmarkGroup, secondary VCFBenchmarkGroup, reciprocal int, overlap int) ([]VCFBenchmark, []VCFBenchmark, []VCFBenchmark, []VCFBenchmark) {
	var match, unmatch, secondaryMatch, secondaryUnmatch []VCFBenchmark
	if master.Svtype == "DEL" {
		match, unmatch, secondaryMatch, secondaryUnmatch = recepicalBench(master.VCFBenchmark, secondary.VCFBenchmark, reciprocal)
	} else if master.Svtype == "DUP" {
		match, unmatch, secondaryMatch, secondaryUnmatch = recepicalBench(master.VCFBenchmark, secondary.VCFBenchmark, reciprocal)
	} else if master.Svtype == "INV" {
		match, unmatch, secondaryMatch, secondaryUnmatch = recepicalBench(master.VCFBenchmark, secondary.VCFBenchmark, reciprocal)
	} else if master.Svtype == "INS" {
		match, unmatch, secondaryMatch, secondaryUnmatch = benchmarkInsertion(master.VCFBenchmark, secondary.VCFBenchmark, overlap)
	} else if master.Svtype == "BND" {
		match, unmatch, secondaryMatch, secondaryUnmatch = benchmarkTranslocation(master.VCFBenchmark, secondary.VCFBenchmark, overlap)
	}

	return match, unmatch, secondaryMatch, secondaryUnmatch
}

func SplitBySvtype(vcflist *[]VCFBenchmark) []VCFBenchmarkGroup {
	var group []VCFBenchmarkGroup

	for _, x := range *vcflist {
		var found bool
		for i := 0; i < len(group); i++ {
			if group[i].Svtype == x.Svtype {
				group[i].VCFBenchmark = append(group[i].VCFBenchmark, x)
				found = true
				break
			}
		}

		if !found {
			var tempgroup VCFBenchmarkGroup
			tempgroup.Svtype = x.Svtype
			tempgroup.VCFBenchmark = append(tempgroup.VCFBenchmark, x)
			group = append(group, tempgroup)
		}
	}

	return group
}

type VCFBenchmarkGroup struct {
	Svtype       string
	VCFBenchmark []VCFBenchmark
}

// return primarymatch,primaryunmatch,secodarymatch,secondaryunmatch
func benchmarkTranslocation(master, slave []VCFBenchmark, overlap int) ([]VCFBenchmark, []VCFBenchmark, []VCFBenchmark, []VCFBenchmark) {
	bv1A, bv1N := compareTranslocationBench(&master, &slave, overlap)

	var unmatch []VCFBenchmark
	var match []VCFBenchmark

	for _, x := range master {
		if x.Verify {
			match = append(match, x)
		} else {
			unmatch = append(unmatch, x)
		}
	}

	return match, unmatch, bv1A, bv1N
}

func compareTranslocationBench(master, slave *[]VCFBenchmark, overlap int) ([]VCFBenchmark, []VCFBenchmark) {
	var temp []VCFBenchmark
	var tempNoAdd []VCFBenchmark
	for _, x := range *slave {
		var added bool
		for y := 0; y < len(*master); y++ {
			if x.Chrom == (*master)[y].Chrom {
				if checkBetween(x.Pos, (*master)[y].Pos, overlap, overlap) && checkBetween(x.End, (*master)[y].End, overlap, overlap) {
					temp = append(temp, x)
					added = true
					(*master)[y].Verify = true
					// break
				}
			}
		}

		for y := 0; y < len(*master); y++ {
			if x.ChromEnd == (*master)[y].Chrom {
				if checkBetween(x.End, (*master)[y].Pos, overlap, overlap) && checkBetween(x.Pos, (*master)[y].End, overlap, overlap) {
					temp = append(temp, x)
					added = true
					(*master)[y].Verify = true
					// break
				}
			}
		}

		if !added {
			tempNoAdd = append(tempNoAdd, x)
		}
	}

	return temp, tempNoAdd
}

// return primarymatch,primaryunmatch,secodarymatch,secondaryunmatch
func benchmarkInsertion(master, slave []VCFBenchmark, overlap int) ([]VCFBenchmark, []VCFBenchmark, []VCFBenchmark, []VCFBenchmark) {

	bv1A, bv1N := compareInsertionBench(&master, &slave, overlap)

	var unmatch []VCFBenchmark
	var match []VCFBenchmark

	for _, x := range master {
		if x.Verify {
			match = append(match, x)
		} else {
			unmatch = append(unmatch, x)
		}
	}

	// fmt.Println(len(match), len(unmatch), len(bv1A), len(bv1N))

	return match, unmatch, bv1A, bv1N
}

func compareInsertionBench(b1, b2 *[]VCFBenchmark, overlap int) ([]VCFBenchmark, []VCFBenchmark) {
	var temp []VCFBenchmark
	var tempNoAdd []VCFBenchmark
	for _, x := range *b2 {
		var added bool
		for y := 0; y < len(*b1); y++ {
			if x.Chrom == (*b1)[y].Chrom {
				if checkBetween(x.Pos, (*b1)[y].Pos, overlap, overlap) {
					temp = append(temp, x)
					added = true
					(*b1)[y].Verify = true
				}
			}
		}

		if !added {
			tempNoAdd = append(tempNoAdd, x)
		}
	}

	return temp, tempNoAdd
}

// return primarymatch,primaryunmatch,secodarymatch,secondaryunmatch
func recepicalBench(master, slave []VCFBenchmark, recepical int) ([]VCFBenchmark, []VCFBenchmark, []VCFBenchmark, []VCFBenchmark) {
	bv1A, bv1N := compareReciprocal(&master, &slave, recepical)

	var unmatch []VCFBenchmark
	var match []VCFBenchmark
	for _, x := range master {
		if x.Verify {
			match = append(match, x)
		} else {
			unmatch = append(unmatch, x)
		}
	}

	return match, unmatch, bv1A, bv1N
}

func compareReciprocal(master, slave *[]VCFBenchmark, overlap int) ([]VCFBenchmark, []VCFBenchmark) {
	var temp []VCFBenchmark
	var tempNoAdd []VCFBenchmark
	for _, x := range *slave {
		var added bool
		for y := 0; y < len(*master); y++ {
			if x.Chrom == (*master)[y].Chrom {
				if reciprocalValidation(x.Pos, x.End, overlap, (*master)[y].Pos, (*master)[y].End) {
					temp = append(temp, x)
					added = true
					(*master)[y].Verify = true

				}
			}
		}

		if !added {
			tempNoAdd = append(tempNoAdd, x)
		}
	}

	return temp, tempNoAdd
}

func reciprocalValidation(pos, end, overlapped, TargetPos, TargetEnd int) bool {

	if pos == TargetPos && end == TargetEnd {
		return true
	} else if pos < TargetPos && end > TargetEnd {
		coverage := float64(TargetEnd-TargetPos) / float64(end-pos)
		if coverage < float64(overlapped)/100 {
			return false
		}

		return true
	} else if (pos >= TargetPos && pos <= TargetEnd) && end > TargetEnd {

		coverage := float64(TargetEnd-pos) / float64(end-pos)
		if coverage < float64(overlapped)/100 {
			return false
		}

		return true
	} else if pos <= TargetPos && (end <= TargetEnd && end >= TargetPos) {
		coverage := float64(end-TargetPos) / float64(end-pos)
		if coverage < float64(overlapped)/100 {
			return false
		}
		return true
	} else if (pos >= TargetPos && pos <= TargetEnd) && end <= TargetEnd {
		coverage := float64(end-pos) / float64(TargetEnd-TargetPos)
		if coverage < float64(overlapped)/100 {
			return false
		}
		return true
	}

	if pos != 0 {
		return false
	}

	if end != 0 {
		return false
	}

	if overlapped != 0 {
		return false
	}

	if TargetPos != 0 {
		return false
	}

	if TargetEnd != 0 {
		return false
	}

	log.Println("reciprocal error", pos, end, overlapped, TargetPos, TargetEnd)
	panic("reciprocal error")
}

func checkBetween(main, compare, negativelimitrange, pluslimitrange int) bool {
	if main-negativelimitrange <= compare && compare <= main+pluslimitrange {
		return true
	}
	return false
}

func removeDuplicatePosRange(master *[]VCFBenchmarkGroup, length int) {
	var group []VCFBenchmarkGroup
	for _, x := range *master {
		temp := removeDuplicatePosEndRange(&x.VCFBenchmark, length)
		// temp := removeDuplicatePosEnd(&x.VCFBenchmark)

		x.VCFBenchmark = temp
		group = append(group, x)
	}

	*master = group
}

func removeDuplicatePosEnd(master *[]VCFBenchmark) []VCFBenchmark {
	var tempAdd []VCFBenchmark
	for _, x := range *master {
		var added bool
		for _, y := range tempAdd {
			if x.Chrom == y.Chrom {
				if x.Pos == y.Pos && x.End == y.End {
					added = true
					break
				}
			}
		}

		if !added {
			tempAdd = append(tempAdd, x)
		}

	}

	return tempAdd
}

func removeDuplicatePosEndRange(master *[]VCFBenchmark, maxrange int) []VCFBenchmark {
	var tempAdd []VCFBenchmark
	for _, x := range *master {
		var added bool
		for _, y := range tempAdd {
			if x.Chrom == y.Chrom {
				if checkBetween(x.Pos, y.Pos, maxrange, maxrange) && checkBetween(x.End, y.End, maxrange, maxrange) {
					added = true
					break
				}
			}
		}

		if !added {
			tempAdd = append(tempAdd, x)
		}

	}

	return tempAdd
}
