package variant

import (
	"fmt"
)

func clearVerifyBench(benchmark *[]VCFBenchmark) {
	for i := 0; i < len(*benchmark); i++ {
		(*benchmark)[i].Verify = false
	}
}

func CreateVennDiagram(one string, two string, three string, svtype string) {
	// master := LoadVCFBenchmark(primaryfile, false, "")
	// master = getOnlySvtype(&master, svtype)
	// master = removeDuplicatePosEndRange(&master, 0)
	// var masterG VCFBenchmarkGroup
	// masterG.Svtype = svtype
	// masterG.VCFBenchmark = master
	// limitSVLength(&masterG, 50)

	onevcf := LoadVCFBenchmark(one, false, "")
	onevcf = getOnlySvtype(&onevcf, svtype)
	onevcf = removeDuplicatePosEndRange(&onevcf, 0)
	var firstG VCFBenchmarkGroup
	firstG.Svtype = svtype
	firstG.VCFBenchmark = onevcf
	limitSVLength(&firstG, 50)

	twovcf := LoadVCFBenchmark(two, false, "")
	twovcf = getOnlySvtype(&twovcf, svtype)
	twovcf = removeDuplicatePosEndRange(&twovcf, 0)
	var secondG VCFBenchmarkGroup
	secondG.Svtype = svtype
	secondG.VCFBenchmark = twovcf
	limitSVLength(&secondG, 50)

	threevcf := LoadVCFBenchmark(three, false, "")
	threevcf = getOnlySvtype(&threevcf, svtype)
	threevcf = removeDuplicatePosEndRange(&threevcf, 0)
	var thirdG VCFBenchmarkGroup
	thirdG.Svtype = svtype
	thirdG.VCFBenchmark = threevcf
	limitSVLength(&thirdG, 50)
	// fmt.Println(len(master),len(onevcf),len(twovcf),len(threevcf))

	overlappedGraph(firstG.VCFBenchmark, secondG.VCFBenchmark, thirdG.VCFBenchmark)
}

func getOnlySvtype(vcflist *[]VCFBenchmark, svtype string) []VCFBenchmark {
	var temp []VCFBenchmark

	for _, x := range *vcflist {
		if x.Svtype == svtype {
			temp = append(temp, x)
		}
	}

	return temp
}

func overlappedGraph(matchedFirst []VCFBenchmark, matchedSecond []VCFBenchmark, matchedThird []VCFBenchmark) {
	// fmt.Println("# Primary Total :", len(benchmark))

	// var matchedFirst []VCFBenchmark
	// if svtype == "DEL" || svtype == "DUP" || svtype == "INV" {
	// 	matchedFirst, _, _, _ = recepicalBench(benchmark, one, 90)
	// } else if svtype == "INS" {
	// 	matchedFirst, _, _, _ = benchmarkInsertion(benchmark, one, 300)
	// }

	totalFirst := len(matchedFirst)
	// fmt.Println("# First Total :", totalFirst)

	// clearVerifyBench(&benchmark)

	// var matchedSecond []VCFBenchmark

	// if svtype == "DEL" || svtype == "DUP" || svtype == "INV" {
	// 	matchedSecond, _, _, _ = recepicalBench(benchmark, two, 90)
	// } else if svtype == "INS" {
	// 	matchedSecond, _, _, _ = benchmarkInsertion(benchmark, two, 300)
	// }

	// clearVerifyBench(&matchedSecond)
	totalSecond := len(matchedSecond)
	// fmt.Println("# Second Total :", totalSecond)

	// clearVerifyBench(&benchmark)

	// var matchedThird []VCFBenchmark

	// if svtype == "DEL" || svtype == "DUP" || svtype == "INV" {
	// 	matchedThird, _, _, _ = recepicalBench(benchmark, three, 90)
	// } else if svtype == "INS" {
	// 	matchedThird, _, _, _ = benchmarkInsertion(benchmark, three, 300)
	// }

	// clearVerifyBench(&matchedThird)
	totalThird := len(matchedThird)
	// fmt.Println("# Third Total :", totalThird)

	// clearVerifyBench(&benchmark)

	clearVerifyBench(&matchedFirst)
	clearVerifyBench(&matchedThird)
	firstNthird, _, _, _ := recepicalBench(matchedFirst, matchedThird, 100)
	clearVerifyBench(&firstNthird)
	totalfirstNthird := len(firstNthird)
	// fmt.Println("# First And Third Total :", totalfirstNthird)

	clearVerifyBench(&matchedFirst)
	clearVerifyBench(&matchedSecond)
	firstNsecond, _, _, _ := recepicalBench(matchedFirst, matchedSecond, 100)
	clearVerifyBench(&firstNsecond)
	totalfirstNsecond := len(firstNsecond)
	// fmt.Println("# First And Second Total :", totalfirstNsecond)

	clearVerifyBench(&matchedSecond)
	clearVerifyBench(&matchedThird)
	secondNthird, _, _, _ := recepicalBench(matchedSecond, matchedThird, 100)
	clearVerifyBench(&secondNthird)
	totalsecondNthird := len(secondNthird)
	// fmt.Println("# Second And Third Total :", totalsecondNthird)

	clearVerifyBench(&secondNthird)
	clearVerifyBench(&firstNsecond)
	tripleMatch, _, _, _ := recepicalBench(secondNthird, firstNsecond, 100)
	clearVerifyBench(&tripleMatch)
	totaltripleMatch := len(tripleMatch)

	secondOnly := totalSecond - (totalfirstNsecond - totaltripleMatch) - (totalsecondNthird - totaltripleMatch) - totaltripleMatch
	firstOnly := totalFirst - (totalfirstNsecond - totaltripleMatch) - (totalfirstNthird - totaltripleMatch) - totaltripleMatch
	thirdOnly := totalThird - (totalsecondNthird - totaltripleMatch) - (totalfirstNthird - totaltripleMatch) - totaltripleMatch

	// +-------------------+
	// |  Second    Third  |
	// |                   |
	// |       First       |
	// +-------------------+
	fmt.Printf("%v,%v,%v,%v,%v,%v,%v\n",
		secondOnly,
		thirdOnly,
		totalsecondNthird-totaltripleMatch,
		firstOnly,
		totalfirstNsecond-totaltripleMatch,
		totalfirstNthird-totaltripleMatch,
		totaltripleMatch,
	)

}
