package variant

import (
	// "fmt"
	"log"

	"github.com/sakkayaphab/vcfgo"
)

func readVCFFile(filepath string) vcfgo.VCF {
	vcf, err := vcfgo.ReadFile(filepath)
	if err != nil {
		log.Fatal(err)
	}
	return vcf
}

func convertVCFToVCFBenchmark(vcf vcfgo.VCF, onlySvtype string) []VCFBenchmark {
	var benchamrkvcf []VCFBenchmark
	for _, x := range vcf.Record {
		pos, err := x.GetPos()
		if err != nil {
			log.Fatal(err)
		}
		temp := VCFBenchmark{
			Chrom: x.GetChrom(),
			Pos:   pos,
		}

		end, err := x.GetEnd()
		if err != nil {
			log.Fatal(err)
		}
		temp.End = end

		chrEnd, err := x.GetChromEnd()
		if err != nil {
			log.Fatal(err)
		}
		temp.ChromEnd = chrEnd

		tempSvtype, err := x.GetSvtype()
		if err != nil {
			log.Fatal(err)
		}
		temp.Svtype = tempSvtype

		if onlySvtype != "" {
			if temp.Svtype != onlySvtype {
				continue
			}
		}

		temp.Source = x.Source

		benchamrkvcf = append(benchamrkvcf, temp)
	}

	return benchamrkvcf
}
