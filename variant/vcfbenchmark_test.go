package variant

import "testing"

func TestExtractInfoFromDEL(t *testing.T) {
	line := "chr1	6941987	DEMOTOOL	A	<DEL>	682	PASS	END=6951925;SVTYPE=DEL;SVLEN=-9938	GT:FT:GQ:PL:PR	1/1:PASS:96:735,99,0:0,34"
	vcf, err := extractLineToVcfBenchmark(line)
	if err != nil {
		t.Error(err)
	}

	if vcf.Chrom != "chr1" {
		t.Error("incorrect Chrom")
	}

	if vcf.ChromEnd != "chr1" {
		t.Error("incorrect ChromEnd :", vcf.ChromEnd)
	}

	if vcf.Pos != 6941987 {
		t.Error("incorrect Pos")
	}

	if vcf.End != 6951925 {
		t.Error("incorrect End")
	}

	if vcf.Svtype != "DEL" {
		t.Error("incorrect Svtype")
	}

}

func TestExtractSVTypeFromColumn4(t *testing.T) {
	line := "1	869574	.	.	<DEL>	.	.	END=870101	SPR:EPR:SEV:EEV:SRD:ERD:SCO:ECO:SOT:EOT:SFR:SLR:EFR:ELR	1.210433e-06:4.960000e-08:4.0:4.0:9:5:2:0:0:0:869142:869302:870269:870330"
	vcf, err := extractLineToVcfBenchmark(line)
	if err != nil {
		t.Error(err)
	}

	if vcf.Chrom != "1" {
		t.Error("incorrect Chrom")
	}

	if vcf.ChromEnd != "1" {
		t.Error("incorrect ChromEnd :", vcf.ChromEnd)
	}

	if vcf.Pos != 869574 {
		t.Error("incorrect Pos")
	}

	if vcf.End != 870101 {
		t.Error("incorrect End")
	}

	if vcf.Svtype != "DEL" {
		t.Error("incorrect Svtype")
	}

}

func TestExtractInfoFromBND(t *testing.T) {
	line := "chr1	4513996	DEMOTOOL	G	]chr7:15064183]G	999	PASS	SVTYPE=BND;IMPRECISE	GT:FT:GQ:PL:PR	1/1:PASS:129:999,132,0:0,35"
	vcf, err := extractLineToVcfBenchmark(line)
	if err != nil {
		t.Error(err)
	}

	if vcf.Chrom != "chr1" {
		t.Error("incorrect Chrom")
	}

	if vcf.ChromEnd != "chr7" {
		t.Error("incorrect ChromEnd :", vcf.ChromEnd)
	}

	if vcf.Pos != 4513996 {
		t.Error("incorrect Pos")
	}

	if vcf.End != 15064183 {
		t.Error("incorrect End :", vcf.End)
	}

	if vcf.Svtype != "BND" {
		t.Error("incorrect Svtype")
	}

}

func TestExtractInfoFromBNDType2(t *testing.T) {
	line := "chr20	20343206	DEMOTOOL	N	<TRA>	.	LowQual	PRECISE;SVTYPE=TRA;CHR2=chr18;END=52268966;SVLEN=31925760	GT:GL:GQ:FT:RC:DR:DV:RR:RV	1/1"
	vcf, err := extractLineToVcfBenchmark(line)
	if err != nil {
		t.Error(err)
	}

	if vcf.Chrom != "chr20" {
		t.Error("incorrect Chrom")
	}

	if vcf.ChromEnd != "chr18" {
		t.Error("incorrect ChromEnd :", vcf.ChromEnd)
	}

	if vcf.Pos != 20343206 {
		t.Error("incorrect Pos")
	}

	if vcf.End != 52268966 {
		t.Error("incorrect End :", vcf.End)
	}

	if vcf.Svtype != "BND" {
		t.Error("incorrect Svtype")
	}

}

func TestExtractInfoFromINSType2(t *testing.T) {
	line := "1	41835544	.	.	<INS>	.	.	END=0	SPR:SEV:SRD:SCO:ECO:SOT:EOT:SSC:HP	0.000000e+00:22.0:84:4:0:0:0:6:1"
	vcf, err := extractLineToVcfBenchmark(line)
	if err != nil {
		t.Error(err)
	}

	if vcf.Chrom != "1" {
		t.Error("incorrect Chrom")
	}

	if vcf.ChromEnd != "1" {
		t.Error("incorrect ChromEnd :", vcf.ChromEnd)
	}

	if vcf.Pos != 41835544 {
		t.Error("incorrect Pos")
	}

	if vcf.End != 0 {
		t.Error("incorrect End :", vcf.End)
	}

	if vcf.Svtype != "INS" {
		t.Error("incorrect Svtype")
	}

}

func TestExtractInfoFromDELType2(t *testing.T) {
	line := "1	26489823	216482464:1	C	C[1:26490139[	52	PASS	DISC_MAPQ=59;EVDNC=ASDIS;HOMSEQ=TCTTCCTTCTC;MAPQ=60;MATEID=216482464:2;MATENM=0;NM=3;NUMPARTS=2;SCTG=c_1_26484501_26509501_2C;SPAN=316;SVTYPE=DEL	GT:AD:DP:GQ:PL:SR:DR:LR:LO	4	0	18	1/1:18:6:4.8:52.8,4.8,0:4:15:-52.81:52.81"
	vcf, err := extractLineToVcfBenchmark(line)
	if err != nil {
		t.Error(err)
	}

	if vcf.Chrom != "1" {
		t.Error("incorrect Chrom")
	}

	if vcf.ChromEnd != "1" {
		t.Error("incorrect ChromEnd :", vcf.ChromEnd)
	}

	if vcf.Pos != 26489823 {
		t.Error("incorrect Pos")
	}

	if vcf.End != 26490139 {
		t.Error("incorrect End :", vcf.End)
	}

	if vcf.Svtype != "DEL" {
		t.Error("incorrect Svtype")
	}

}
