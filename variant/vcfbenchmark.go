package variant

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
	"strings"
)

type VCFBenchmark struct {
	Chrom    string
	Pos      int
	ChromEnd string
	End      int
	Svtype   string
	Source   *string
	Verify   bool
}

func LoadVCFBenchmark(filepath string, keepsource bool, onlysvtype string) []VCFBenchmark {
	file, err := os.Open(filepath)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	reader := bufio.NewReaderSize(file, 1024*10000)
	var benchamrkvcf []VCFBenchmark
	for {
		line, _, err := reader.ReadLine()

		if err == io.EOF {
			break
		}

		if string(line)[0:1] == "#" {
			continue
		}

		temp, err := extractLineToVcfBenchmark(string(line))
		if err != nil {
			log.Fatal(err)
		}

		// if keepsource {
		// 	temp.Source = string(line)
		// }

		if onlysvtype == "" {
			benchamrkvcf = append(benchamrkvcf, temp)
		} else if onlysvtype == temp.Svtype {
			benchamrkvcf = append(benchamrkvcf, temp)
		}

	}

	return benchamrkvcf
}

func extractLineToVcfBenchmark(line string) (VCFBenchmark, error) {

	s := strings.Split(string(line), "\t")

	if len(s) < 7 {
		return VCFBenchmark{}, errors.New("not VCF Format")
	}

	var vcf VCFBenchmark

	vcf.Chrom = s[0]
	pos, err := strconv.Atoi(s[1])
	if err != nil {
		log.Fatal(err)
	}
	vcf.Pos = pos

	infos := strings.Split(s[7], ";")

	for _, x := range infos {
		if strings.Split(x, "=")[0] == "END" {

			endS := strings.Split(x, "=")[1]
			end, err := strconv.Atoi(endS)
			if err != nil {
				panic("error")
			}
			vcf.End = end
		}

		if strings.Split(x, "=")[0] == "SVTYPE" {
			vcf.Svtype = strings.Split(x, "=")[1]
		}

		if strings.Split(x, "=")[0] == "CHR2" {
			vcf.ChromEnd = strings.Split(x, "=")[1]
		}
	}

	if vcf.Svtype == "" {
		svtype, err := findSVtypeInColumnFour(s[4])
		if err != nil {

		} else {
			vcf.Svtype = svtype
		}
	}

	if vcf.Svtype == "TRA" {
		vcf.Svtype = "BND"
	}

	if vcf.Svtype == "BND" {
		tempChrEnd, tempEnd, err := getChromAndEndFromAlt(s[4])
		if err != nil {

		}

		if tempChrEnd != "" {
			vcf.ChromEnd = tempChrEnd
		}

		if vcf.End == 0 {
			vcf.End = tempEnd
		}

	} else {
		vcf.ChromEnd = vcf.Chrom
	}

	// Specific version
	if true {
		_, tempEnd, _ := getChromAndEndFromAlt(s[4])
		if vcf.End == 0 {
			vcf.End = tempEnd
		}

	}
	// End specific version

	if vcf.Svtype == "" {

		fmt.Println(line)
		return VCFBenchmark{}, errors.New("not found SVTYPE [extractLineToVcfBenchmark]")
	}

	return vcf, nil
}

func findSVtypeInColumnFour(text string) (string, error) {

	text = strings.Trim(text, "<>")

	if checkStringInSvtypeList(text) {
		return text, nil
	}

	return text, errors.New("not found SVTYPE")
}

func checkStringInSvtypeList(a string) bool {
	var list = []string{"DEL", "DUP", "INV", "INS", "BND"}
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

func getChromAndEndFromAlt(text string) (string, int, error) {
	//remove first
	var firstRemove int
	for mi, m := range text {
		// fmt.Println(string(m), mi)
		if m == '[' || m == ']' {
			firstRemove = mi
			break
		}
	}

	var lastRemove int
	for mi, m := range text {
		// fmt.Println(string(m), mi)
		if mi <= firstRemove {
			continue
		}

		if m == '[' || m == ']' {
			lastRemove = mi
			break
		}
	}

	if lastRemove == 0 {
		return "", 0, errors.New("input invalid")
	}

	s := strings.Split(text[firstRemove+1:lastRemove], ":")
	chrS := s[0]
	posS := s[1]
	pos, err := strconv.Atoi(posS)
	if err != nil {
		panic("stop")
	}

	if chrS == "" {
		return "", 0, errors.New("input invalid")
	}

	return chrS, pos, nil
}
