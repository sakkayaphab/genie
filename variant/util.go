package variant

import "strconv"

func String(n int) string {
	return strconv.Itoa(n)
}
