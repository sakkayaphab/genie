package variant

import (
	// "fmt"
	"log"

	"github.com/sakkayaphab/genie/bed"
)

func readBEDFile(filepath string) bed.BED {
	vcf, err := bed.ReadFile(filepath)
	if err != nil {
		log.Fatal(err)
	}
	return vcf
}

func convertBEDToVCFBenchmark(bed bed.BED, svtype string) []VCFBenchmark {
	var benchamrkvcf []VCFBenchmark
	for _, x := range bed.Record {
		temp := VCFBenchmark{
			Chrom: x.GetChrom(),
			Pos:   x.GetChromStart(),
			End:   x.GetChromEnd(),
		}

		temp.Svtype = svtype

		benchamrkvcf = append(benchamrkvcf, temp)
	}

	return benchamrkvcf
}
