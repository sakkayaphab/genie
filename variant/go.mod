module variant

go 1.13

require (
	github.com/olekukonko/tablewriter v0.0.2
	github.com/sakkayaphab/fastago v0.0.0-20200107222600-a9393bcdb2b1 // indirect
	github.com/sakkayaphab/genie/bed v0.0.0
)

replace github.com/sakkayaphab/genie/bed v0.0.0 => ../bed
